using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;


using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Genkit;
using static Hearthpyre.Static;
//using Rewired;

namespace Hearthpyre.UI
{
	public abstract class TargetView : View
	{
		public Cell ActiveCell;
		public GameObject ActiveObject;
		public char CursorChar;
		public Index CursorSize { get; set; }
		public Index MouseFocus; // TODO: Consider splitting functionality into elements like GovernView.
		public IEnumerable<Cell> CursorArea;

		public TargetView(string name) : base(name) {
			CursorChar = '\u00C5';
			CursorSize = new Index(max: 10);
			MouseFocus = new Index(max: 2);
		}

		public override void Render(ScreenBuffer sb) {
			if (((XRLCore.FrameTimer.ElapsedMilliseconds) & 511) < 256) {
				foreach (var cell in CursorArea) {
					var chr = sb[cell.X, cell.Y];
					chr.Char = CursorChar;
					chr.Foreground = CLB_WHT;
					chr.Background = CLD_BLK;
				}
			}
		}

		public override bool Input(Keys keys) {
			if (base.Input(keys)) return true;

			switch (keys) {
				case Keys.MouseEvent:
					return MouseInput(Keyboard.CurrentMouseEvent);
				case Keys.Control | Keys.NumPad8:
				case Keys.Shift | Keys.NumPad8:
					CursorSize++;
					PlayUISound(SND_MENU);
					Shift(0, 0);
					return true;
				case Keys.Control | Keys.NumPad2:
				case Keys.Shift | Keys.NumPad2:
					CursorSize--;
					PlayUISound(SND_MENU);
					Shift(0, 0);
					return true;
				default: return false;
			}
		}

		public virtual bool MouseInput(Keyboard.MouseEvent me) {
			if (me.Event == "PointerOver") {
				var x = Keyboard.CurrentMouseEvent.x;
				var y = Keyboard.CurrentMouseEvent.y;
				Move(x, y);
			} else if (me.Event == "RightClick") {
				MouseFocus++;
				MenuSound();
			} else if (me.Event == "LeftClick") {
				if (MouseFocus == 0) Input(Keys.Q);
				else if (MouseFocus == 1) Input(Keys.E);
				else Input(Keys.R);
			} else return false;
			return true;
		}

		public override void Move(int x, int y) {
			x = Calc.Clamp(x, 0, ActiveZone.Width - 1);
			y = Calc.Clamp(y, 0, ActiveZone.Height - 1);
			base.Move(x, y);

			ActiveCell = ActiveZone.GetCell(Cursor.x, Cursor.y);
			ActiveObject = ActiveCell.GetHighestRenderLayerObject();
			CursorArea = ActiveCell.LazyCircle(CursorSize);
		}

		public override void Reset() {
			CursorSize.Value = 0;
			var C = GAME.Player?.Body?.CurrentCell;
			if (C != null)
				Cursor = new Point2D(C.X, C.Y);
			else
				Cursor = new Point2D(ActiveZone.X / 2, ActiveZone.Y / 2);
		}
	}
}
