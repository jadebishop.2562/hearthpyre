using System;
using System.Collections.Generic;

using XRL;
using XRL.World;
using XRL.World.Parts;
using XRL.Rules;
using ConsoleLib.Console;

using static Hearthpyre.Static;
//using Detail = UnityEngine.Color;

namespace Hearthpyre.UI
{
	public abstract class Brush
	{
		public const int PAINTS_CELLS = 1;
		public const int PAINTS_OBJECTS = 1 << 1;
		public const int ACTIVE_ICON = 1 << 2;

		public string Name { get; protected set; }
		public int Flags { get; protected set; }
		public ConsoleChar Icon { get; } = new ConsoleChar { Foreground = CLD_BLK, Background = CLD_BLK, Detail = CLD_BLK };

		public void Paint(IEnumerable<Cell> cells) {
			foreach (var cell in cells)
				Paint(cell);
		}
		public void Paint(IEnumerable<GameObject> objects) {
			foreach (var obj in objects)
				Paint(obj);
		}

		public virtual void Paint(Cell cell) { }
		public virtual void Paint(GameObject obj) { }
		//public string Icon;
		//public ushort Attributes;
		//public Detail Detail;
		//public Action<IEnumerable<Cell>> CellAction;
		//public Action<IEnumerable<GameObject>> ObjectAction;

		protected void ClearPaintedObjects(Cell cell) {
			cell.ClearObjectsWithIntProperty(TAG_PNT);
		}

		protected GameObject CreatePaintedObject(string blueprint) {
			var obj = GameObjectFactory.Factory.CreateObject(blueprint);
			obj.SetIntProperty(TAG_PNT, 1);
			return obj;
		}
	}

	// TODO: Set color game state.
	public class ColorBrush : Brush
	{
		string color;
		public string Color {
			get { return color; }
			set {
				color = value;
				Icon.SetColorString(value);
			}
		}

		string detail;
		public string Detail {
			get { return detail; }
			set {
				detail = value;
				Icon.Detail = ColorUtility.ColorMap[value[0]];
			}
		}

		public ColorBrush() {
			Name = "Color";
			Flags = PAINTS_CELLS | PAINTS_OBJECTS | ACTIVE_ICON;
			Icon.Tile = "Icons/hp_gradient_drop.png";
			Icon.Foreground = CLB_BLUE;
			Icon.Detail = CLB_MAG;
		}

		public override void Paint(Cell cell) {
			if (!String.IsNullOrEmpty(Color)) {
				if (!String.IsNullOrEmpty(cell.PaintTileColor))
					cell.PaintTileColor = Color;
				cell.PaintColorString = Color;
			}
			if (!String.IsNullOrEmpty(Detail))
				cell.PaintDetailColor = Detail;
		}

		public override void Paint(GameObject obj) {
			var render = obj.pRender;
			if (!String.IsNullOrEmpty(Color)) {
				if (!String.IsNullOrEmpty(render.TileColor))
					render.TileColor = Color;
				render.ColorString = Color;
			}
			if (!String.IsNullOrEmpty(Detail))
				render.DetailColor = Detail;
		}
	}

	public class FloralBrush : Brush
	{
		protected string[] Colors = new[] { "b", "B", "g", "G", "r", "R", "m", "M", "W", "Y", "K" };

		public FloralBrush() {
			Name = "Floral";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Terrain/tile_flowers2.bmp";
			Icon.Foreground = CLB_BLUE;
			Icon.Detail = CLB_RED;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			cell.PaintTile = "Terrain/tile_flowers" + Stat.RandomCosmetic(1, 2) + ".bmp";
			cell.PaintColorString = "&" + Colors.GetRandomElement();
			cell.PaintDetailColor = Colors.GetRandomElement().ToString();
		}
	}

	// TODO: Inherit from FloralBrush when inheritance > 2 not broken.
	public class FloralBunchedBrush : Brush
	{
		string[] Colors = new[] { "b", "B", "g", "G", "r", "R", "m", "M", "W", "Y", "K" };

		public FloralBunchedBrush() {
			Name = "Floral";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Terrain/sw_flowers_bunched_1.bmp";
			Icon.Foreground = CLB_BLUE;
			Icon.Detail = CLB_MAG;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			cell.PaintTile = "Terrain/sw_flowers_bunched_" + Stat.RandomCosmetic(1, 4) + ".bmp";
			cell.PaintColorString = "&" + Colors.GetRandomElement();
			cell.PaintDetailColor = Colors.GetRandomElement().ToString();
		}
	}

	public class JungleBrush : Brush
	{
		List<string> Objects = PopulationManager.GetEach("DynamicObjectsTable:Jungle_Plants", null);
		bool Tree;

		public JungleBrush() {
			Name = "Jungle";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Terrain/sw_mangrove1.bmp";
			Icon.Foreground = CLD_GRN;
			Icon.Detail = CLD_YEL;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			Grassy.PaintCell(cell);
			Tree |= Stat.RandomCosmetic(1, 100) <= 5;

			if (Tree && !cell.AnyObject()) {
				var obj = GameObjectFactory.Factory.CreateObject(Objects.GetRandomElement());
				obj.SetIntProperty(TAG_PNT, 1);
				cell.AddObject(obj);
				Tree = false;
			}
		}
	}

	public class FungalBrush : Brush
	{
		string[] Objects = new[] { "Spotted Shagspook 1", "Spotted Shagspook 2", "Dandy Cap" };
		string[] Colors = new[] { "B", "C", "c", "Y", "y", "K" };
		bool Shroom;

		public FungalBrush() {
			Name = "Fungal";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Creatures/sw_shroom2.bmp";
			Icon.Foreground = CLD_CYAN;
			Icon.Detail = CLB_CYAN;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			Mushroomy.Paint(cell);
			Shroom |= Stat.RandomCosmetic(1, 100) <= 4;
			if (Shroom && !cell.AnyObject()) {
				var obj = CreatePaintedObject(Objects.GetRandomElement());
				var render = obj.pRender;
				render.TileColor = "&" + Colors.GetRandomElement();
				do { render.DetailColor = Colors.GetRandomElement().ToString(); }
				while (render.TileColor[1] == render.DetailColor[0]);
				cell.AddObject(obj);
				Shroom = false;
			}
		}
	}

	public class MarshBrush : Brush
	{
		public MarshBrush() {
			Name = "Marsh";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Terrain/sw_grass2.bmp";
			Icon.Foreground = CLB_GRN;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			Dirty.PaintCell(cell);
		}
	}

	public class CaveBrush : Brush
	{
		public CaveBrush() {
			Name = "Cave";
			Flags = PAINTS_CELLS;
			Icon.Tile = "assets_content_textures_tiles_tile-dirt.png";
			Icon.Foreground = CLD_WHT;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			cell.PaintColorString = "&y";
			cell.PaintTile = "assets_content_textures_tiles_tile-dirt.png";
			cell.PaintDetailColor = "k";
		}
	}

	public class DesertBrush : Brush
	{
		string[] Colors = new[] { "&y^k", "&c^k" };
		bool Dune;

		public DesertBrush() {
			Name = "Desert";
			Flags = PAINTS_CELLS;
			Icon.Tile = "Terrain/sw_ground_dune_4.bmp";
			Icon.Foreground = CLD_WHT;
		}

		public override void Paint(Cell cell) {
			ClearPaintedObjects(cell);
			Dune |= Stat.RandomCosmetic(1, 100) <= 5;
			if (Dune && !cell.AnyObject()) {
				var obj = GameObjectFactory.Factory.CreateObject("HighSaltDune");
				obj.SetIntProperty(TAG_PNT, 1);

				var render = obj.pRender;
				render.Tile = cell.PaintTile = "Terrain/sw_ground_dune_" + Stat.RandomCosmetic(1, 12) + ".bmp";
				render.TileColor = cell.PaintColorString = Colors.GetRandomElement();
				render.DetailColor = cell.PaintDetailColor = "k";

				cell.AddObject(obj);
				Dune = false;
			} else if (Stat.RandomCosmetic(1, 10) <= 6) {
				cell.PaintTile = "Terrain/sw_ground_desert_1.bmp";
				cell.PaintColorString = "&K^k";
			} else {
				cell.PaintTile = "Terrain/sw_ground_desert_" + Stat.RandomCosmetic(1, 12) + ".bmp";
				cell.PaintColorString = Colors.GetRandomElement();
			}
		}
	}
}
