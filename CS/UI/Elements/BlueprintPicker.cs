using ConsoleLib.Console;
using System;
using System.Linq;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using System.Threading;
using System.Diagnostics;
using Hearthpyre.Realm;
using XRL;
using static Hearthpyre.Static;
using Color = UnityEngine.Color;

namespace Hearthpyre.UI
{
	public class BlueprintPicker : Element
	{
		public const string NAME = "HearthpyreBlueprintPicker";

		public event EventHandler Reload;

		public List<Notitia.Category> Categories { get; private set; }
		public Notitia.Category ActiveCategory { get; private set; }
		public Index IndexCategory { get; private set; }

		public List<Notitia.Blueprint> Blueprints { get; private set; }
		public Notitia.Blueprint ActiveBlueprint { get; private set; }
		public Index IndexBlueprint { get; private set; }

		public Box Box;
		public bool FlipBox;
		public Index Column;
		public int Rows;
		public bool Visible;

		public BlueprintPicker() {
			Rows = 4;
			Box = new Box(0, 0, 22, 8);
		}

		public void Retranslate(int x, int y) {
			Box = Box.Retranslate(x, y);
			FlipBox = Box.y2 > 20;
		}

		public void Enter() {
			var packages = HearthpyreXyloschemer.ACTIVE?.Packages ?? new List<string>();
			Categories = Notitia.Categories.Select(x => x.Value.Filter(packages))
				.Where(x => x.Blueprints.Count > 0)
				.ToList();

			Categories.Sort((x, y) => String.Compare(x.Name, y.Name));

			IndexCategory = new Index(Categories, GAME.GetIntGameState(NAME + "Category", 0));
			IndexBlueprint = new Index();
			Column = new Index(max: 1);
			UpdateCategory();
		}

		public void Update() { }

		public void Render(ScreenBuffer sb) {
			var fill = Box.Grow(-1);
			sb.Fill(fill.x1, fill.y1, fill.x2, fill.y2, 32, 0);
			if (FlipBox) sb.FoldUp(Box.x1, Box.y1, Box.x2, Box.y2);
			else sb.FoldDown(Box.x1, Box.y1, Box.x2, Box.y2);
			//RenderBox(sb, Box.x1, Box.y1, Box.x2, Box.y2);
			//sb.Write(Box.x1 + 2, Box.y1, "[ &WCategory&y ]");
			//sb.Write(Box.x2 - 14, Box.y1, "[ &WBlueprint&y ]");

			// Left column, categories
			int x = Box.x1 + 1, y = Box.MidY;
			RenderLine(sb, x, y, ActiveCategory, Column == 0 ? "&C" : "&y");
			for (int i = 1; i < Rows; i++) {
				var clr = (Column == 1 || i == Rows - 1) ? "&K" : "&y";
				RenderLine(sb, x, y + i, Categories[IndexCategory + i], clr);
				RenderLine(sb, x, y - i, Categories[IndexCategory - i], clr);
			}

			RenderSplitter(sb, Box.x1 + 7, CLD_WHT);

			// Right column, blueprints
			x = Box.x1 + 8;
			RenderLine(sb, x, y, ActiveBlueprint, Column == 1 ? "&C" : "&y");
			for (int i = 1; i < Rows; i++) {
				var clr = (Column == 0 || i == Rows - 1) ? "&K" : "&y";
				RenderLine(sb, x, y + i, Blueprints[IndexBlueprint + i], clr);
				RenderLine(sb, x, y - i, Blueprints[IndexBlueprint - i], clr);
			}

			// Charge cost indicator
			RenderCharge(sb, Box.x2 - 1, Box.y2 - 1);
		}


		void RenderLine(ScreenBuffer sb, int x, int y, Notitia.Category cat, string color = "") {
			sb[x, y].Copy(cat.Icon);
			sb.Write(x + 1, y, color + cat.Name);
		}

		void RenderLine(ScreenBuffer sb, int x, int y, Notitia.Blueprint blp, string color = "") {
			sb[x, y].Copy(blp.Icon);
			sb.Write(x + 1, y, color + blp.Name);
		}

		void RenderSplitter(ScreenBuffer sb, int x, Color color) {
			for (int y = Box.y1; y <= Box.y2; y++) {
				var chr = sb[x, y];
				chr.Foreground = color;
				chr.Background = CLD_BLK;
				if (y == Box.y1) chr.Char = '\u00C2';
				else if (y == Box.y2) chr.Char = '\u00C1';
				else chr.Char = '\u00B3';
			}
		}

		void RenderBox(ScreenBuffer sb, int x1, int y1, int x2, int y2) {
			for (int i = x1; i <= x2; i++) {
				var b = sb[i, y2];

				b.Char = '\u00C4';
				b.Foreground = CLD_WHT;
				b.Background = CLD_BLK;
			}
			for (int i = y1; i <= y2; i++) {
				var l = sb[x1, i];
				var r = sb[x2, i];

				l.Char = '\u00B3';
				l.Foreground = CLD_WHT;
				l.Background = CLD_BLK;
				r.Char = '\u00B3';
				r.Foreground = CLD_WHT;
				r.Background = CLD_BLK;
			}
			sb[x1, y1].Char = '\u00C2';
			sb[x2, y1].Char = '\u00C2';
			sb[x1, y2].Char = '\u00C0';
			sb[x2, y2].Char = '\u00D9';
		}

		void RenderCharge(ScreenBuffer sb, int x, int y) {
			var tier = ActiveBlueprint.Value.Tier;
			var icon = "Icons/hp_charge_1.png";
			var detail = CLB_GRN;

			if (tier > 5) {
				icon = "Icons/hp_charge_3.png";
				detail = CLB_RED;
			} else if (tier > 2) {
				icon = "Icons/hp_charge_2.png";
				detail = CLB_YEL;
			}

			//sb.Write(x - 1, y, " \0 ");
			sb[x, y].Display(icon, CLD_CYAN, detail);
		}

		public bool Input(Keys keys) {
			if (!Visible) return false;

			switch (keys) {
				case Keys.W:
				case Keys.NumPad8:
					if (Column == 0) {
						IndexCategory--;
						UpdateCategory();
					} else {
						IndexBlueprint--;
						UpdateBlueprint();
					}
					break;
				case Keys.S:
				case Keys.NumPad2:
					if (Column == 0) {
						IndexCategory++;
						UpdateCategory();
					} else {
						IndexBlueprint++;
						UpdateBlueprint();
					}
					break;
				case Keys.A:
				case Keys.NumPad4:
					Column--;
					break;
				case Keys.D:
				case Keys.NumPad6:
					Column++;
					break;
				case Keys.Control | Keys.S:
				case Keys.Shift | Keys.S:
					SearchBlueprint();
					return true;
				case Keys.Escape:
				case Keys.Control | Keys.R:
				case Keys.Shift | Keys.R:
				case Keys.R:
					Visible = false;
					break;
				case Keys.Enter:
				case Keys.Space:
					Visible = false;
					Reload.Invoke(this, new EventArgs());
					GAME.SetIntGameState(NAME + "Category", IndexCategory);
					break;
				case Keys.H:
					ShowHelp();
					break;
				default: return true;
			}

			MenuSound();
			return true;
		}

		public void UpdateCategory(int blueprint = -1) {
			ActiveCategory = Categories[IndexCategory];
			Blueprints = ActiveCategory.Blueprints;
			IndexBlueprint.Max = Blueprints.Count - 1;
			IndexBlueprint.Min = 0;
			IndexBlueprint.Value = blueprint >= 0 ? blueprint : GAME.GetIntGameState(NAME + ActiveCategory.Name, 0);
			UpdateBlueprint();
		}

		public void UpdateBlueprint() {
			ActiveBlueprint = Blueprints[IndexBlueprint];
			GAME.SetIntGameState(NAME + ActiveCategory.Name, IndexBlueprint);
		}


		public void SearchBlueprint() {
			var query = Popup.AskString("Search for a blueprint.", "", 100);
			if (string.IsNullOrEmpty(query)) return;

			var indexCategory = -1;
			var indexBlueprint = -1;
			var match = Int32.MaxValue;
			for (int ic = 0; ic < Categories.Count; ic++) {
				var category = Categories[ic];
				for (int ib = 0; ib < category.Blueprints.Count; ib++) {
					var blueprint = category.Blueprints[ib];
					var m = Static.StrDist(query, blueprint.Name);
					var display = blueprint.Value.CachedDisplayNameStripped;
					if (!String.IsNullOrEmpty(display)) m = Math.Min(m, Static.StrDist(query, display));

					if (match > m) {
						indexCategory = ic;
						indexBlueprint = ib;
						match = m;
					}
				}
			}

			if (indexCategory >= 0 && indexBlueprint >= 0) {
				IndexCategory.Value = indexCategory;
				Column.Value = 1;
				UpdateCategory(indexBlueprint);
				if (!Visible) {
					Reload.Invoke(this, new EventArgs());
					GAME.SetIntGameState(NAME + "Category", IndexCategory);
				}
				MenuSound();
			} else {
				PlayUISound(SND_ERR);
			}
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|BLUEPRINT HELP}}") + "\n" +
				"Press the {{W|W}}, {{W|A}}, {{W|S}}, {{W|D}} or {{W|Arrow}} keys to navigate the blueprints.\n" +
				"Categories are displayed in the left column and blueprints within said category in the right column.\n\n" +
				"{{W|Shift}} + {{W|S}} to search for a blueprint by name.\n\n" +
				"{{W|Space}} or {{W|Enter}} to confirm your selection, {{W|Esc}} to cancel.";

			Popup.Show(text);
		}

		public void Leave() { }
	}
}
