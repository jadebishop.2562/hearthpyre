using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;

namespace Hearthpyre.UI
{
	// TODO: Consider makin' this abstract class to shove some basic UI methods here.
	// A lot of picker functionality could be handled generally here.
	// Once mono compiler can handle inheritance > 2.
	public interface Element
	{
		event EventHandler Reload;
		// bool FlipBox

		void Enter();
		void Update();
		void Render(ScreenBuffer sb);
		bool Input(Keys keys);
		void Leave();
	}
}
