using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using XRL.UI;
using System.Threading;
using System.Diagnostics;
using Hearthpyre.Realm;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	public class MusicPicker : Element
	{
		Index MusicIndex;
		bool Disabled;
		bool ActiveFrame;

		public event EventHandler Reload { add { } remove { } }
		public HearthpyreGoverning Governor;
		public Zone ActiveZone;
		public Settlement ActiveVillage;
		public GameObject ActivePerformer;
		public Notitia.Entry ActiveEntry;

		public MusicPicker() {
		}

		public void Enter() {
			var player = GAME.Player.Body;
			Governor = player.GetPart<HearthpyreGoverning>();

			ActiveZone = GAME.ZoneManager.ActiveZone;
			ActiveVillage = Governor?.GetSettlement(ActiveZone);
			ActivePerformer = ActiveZone.GetFirstObject("ZoneMusic");

			Disabled = ActiveVillage == null || !player.HasSkill(GOV_HRLD);

			var track = ActivePerformer?.GetStringProperty("Track");
			MusicIndex = new Index(Notitia.Music, Notitia.Music.FindIndex(x => x.Value == track));
			if (MusicIndex.Value == -1) {
				var next = SoundManager.nextTrack;
				ActiveEntry = new Notitia.Entry(next.Spaceify(), next);
			} else {
				ActiveEntry = Notitia.Music[MusicIndex];
			}
		}

		public void Update() {
			ActiveFrame = true;
		}

		public void Render(ScreenBuffer sb) {
			int x = sb.X - 3, y = sb.Y;

			var entryPos = (int)Math.Round(x + 3.5 - ActiveEntry.Name.Quantify() * 0.5);
			sb.Write(entryPos, y, (Disabled ? "{{K|" : "{{y|") + ActiveEntry.Name + "}}");

			if (ActiveFrame) {
				if (Disabled) sb.Write(x, y + 1, "{{K|<A {{C|\u000E}} D>}}");
				else sb.Write(x, y + 1, "{{y|<{{W|A}} {{C|\u000E}} {{W|D}}>}}");
				ActiveFrame = false;
			} else {
				sb.Write(x, y + 1, "   {{C|\u000E}}   ");
			}
		}

		public bool Input(Keys keys) {
			if (keys == Keys.H) {
				ShowHelp();
				return true;
			}

			if (Disabled) return false;
			switch (keys) {
				case Keys.A:
					MusicIndex--;
					ActiveEntry = Notitia.Music[MusicIndex];
					PlayUISound(SND_MENU);
					break;
				case Keys.D:
					MusicIndex++;
					ActiveEntry = Notitia.Music[MusicIndex];
					PlayUISound(SND_MENU);
					break;
				case Keys.Shift | Keys.A:
					Seek(-5); break;
				case Keys.Control | Keys.A:
					Seek(-25); break;
				case Keys.Shift | Keys.D:
					Seek(5); break;
				case Keys.Control | Keys.D:
					Seek(25); break;
				case Keys.Control | Keys.R:
					Enter(); break;
				default: return false;
			}

			SoundManager.PlayMusic(ActiveEntry.Value, true);

			return true;
		}

		public void Seek(float time) {
			GameManager.Instance.uiQueue.queueTask(() => {
				var source = SoundManager.MusicSource.GetComponent<UnityEngine.AudioSource>();
				if (source.time + time > source.clip.length)
					source.time += time - source.clip.length;
				else if (source.time + time < 0f)
					source.time = 0f;
				else
					source.time += time;
			});
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|MUSIC HELP}} \u00c4 {{C|" + (ActiveEntry.Detail ?? ActiveEntry.Name) + "}}") + "\n";
			if (!PLAYER.HasSkill(GOV_HRLD)) text += "{{R|Requires the Heraldry skill.}}\n\n";

			text += "Press the {{W|A}} and {{W|D}} keys to crossfade between songs.\n\n" +
			   "Hold the {{W|Shift}} or {{W|Control}} modifiers while pressing {{W|A}} or {{W|D}} to seek backwards and forwards." +
			   "\n\nPress {{W|Control}} + {{W|R}} to reset the value.";

			Popup.Show(text);
		}

		public void Leave() {
			if (Disabled) return;
			if (ActivePerformer == null)
				ActivePerformer = ActiveZone.GetCell(0, 0).AddObject("ZoneMusic");

			ActivePerformer.SetStringProperty("Track", ActiveEntry.Value);
		}
	}
}
