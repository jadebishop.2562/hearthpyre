using ConsoleLib.Console;
using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.Skills;
using Hearthpyre.Realm;
using XRL.UI;
using System.Diagnostics;
using Color = UnityEngine.Color;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	public class IconPicker : Element
	{
		static char[] colors = new[]{
			'k', 'K', 'b', 'B',
			'g', 'G', 'c', 'C',
			'r', 'R', 'm', 'M',
			'w', 'W', 'y', 'Y',
			'o', 'O'
		};

		StringComparer Comparer = StringComparer.OrdinalIgnoreCase;
		bool ActiveFrame;

		public event EventHandler Reload { add { } remove { } }
		public Index ForeIndex;
		public Index BackIndex;
		public Index DetailIndex;
		public Index IconIndex;
		public bool Disabled;

		public Color Foreground;
		public Color Background;
		public Color Detail;
		public HearthpyreGoverning Governor;

		public Settlement ActiveVillage;
		public Render ActiveRender;
		public Notitia.Entry ActiveEntry;

		public IconPicker() {
			Foreground = CLB_YEL;
			Background = CLD_BLK;
			Detail = CLB_WHT;
		}

		public void Enter() {
			var player = GAME.Player.Body;
			Governor = player.GetPart<HearthpyreGoverning>();

			ActiveVillage = Governor?.GetSettlement(GAME.ZoneManager.ActiveZone);
			ActiveRender = ActiveVillage?.Terrain?.pRender;

			Disabled = ActiveRender == null || !player.HasSkill(GOV_HRLD);

			IconIndex = new Index(Notitia.Icons);
			ActiveEntry = Notitia.Icons.GetRandomElement();
			if (ActiveRender != null) {
				IconIndex.Value = Notitia.Icons.FindIndex(x => Comparer.Equals(x.Value, ActiveRender.Tile));
				if (IconIndex != -1) ActiveEntry = Notitia.Icons[IconIndex];
				FillColorIndexes();
			}
		}

		public void Update() {
			ActiveFrame = true;
		}

		public void Render(ScreenBuffer sb) {
			int x = sb.X - 3, y = sb.Y;

			var entryPos = RoundToInt(x + 3.5 - ActiveEntry.Name.Quantify() * 0.5);
			sb.Write(entryPos, y, (Disabled ? "{{K|" : "{{y|") + ActiveEntry.Name + "}}");

			if (ActiveFrame) {
				if (Disabled) sb.Write(x, y + 1, "{{K|<A \0 D>}}");
				else sb.Write(x, y + 1, "{{y|<{{W|A}} \0 {{W|D}}>}}");
				ActiveFrame = false;
			} else {
				sb.Write(x, y + 1, "   \0   ");
			}

			var chr = sb[x + 3, y + 1];
			chr.Tile = ActiveEntry.Value;
			chr.Foreground = Foreground;
			chr.Background = Background;
			chr.Detail = Detail;
		}

		public bool Input(Keys keys) {
			if (keys == Keys.H) {
				ShowHelp();
				return true;
			}

			if (Disabled) return false;
			switch (keys) {
				case Keys.A:
					IconIndex--;
					ActiveEntry = Notitia.Icons[IconIndex];
					break;
				case Keys.D:
					IconIndex++;
					ActiveEntry = Notitia.Icons[IconIndex];
					break;

				case Keys.Shift | Keys.A:
					ForeIndex++;
					UpdateColors();
					break;
				case Keys.Shift | Keys.S:
					BackIndex++;
					UpdateColors();
					break;
				case Keys.Shift | Keys.D:
					DetailIndex++;
					UpdateColors();
					break;
				case Keys.Control | Keys.A:
					ForeIndex--;
					UpdateColors();
					break;
				case Keys.Control | Keys.S:
					BackIndex--;
					UpdateColors();
					break;
				case Keys.Control | Keys.D:
					DetailIndex--;
					UpdateColors();
					break;
				case Keys.Control | Keys.R:
					Enter();
					break;
				default: return false;
			}

			MenuSound();
			return true;
		}

		public void FillColorIndexes() {
			var color = ActiveRender.ColorString;
			if (!String.IsNullOrEmpty(ActiveRender.TileColor))
				color = ActiveRender.TileColor;

			ForeIndex = new Index(colors);
			var i = color.LastIndexOf('&');
			if (i > -1) ForeIndex.Value = Array.IndexOf(colors, color[i + 1]);

			BackIndex = new Index(colors);
			i = color.LastIndexOf('^');
			if (i > -1) BackIndex.Value = Array.IndexOf(colors, color[i + 1]);

			DetailIndex = new Index(colors);
			if (!String.IsNullOrEmpty(ActiveRender.DetailColor))
				DetailIndex.Value = Array.IndexOf(colors, ActiveRender.DetailColor[0]);

			UpdateColors();
		}

		public void UpdateColors() {
			Foreground = ColorUtility.ColorMap[colors[ForeIndex]];
			Background = ColorUtility.ColorMap[colors[BackIndex]];
			Detail = ColorUtility.ColorMap[colors[DetailIndex]];
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|TILE HELP}}") + "\n";
			if (!PLAYER.HasSkill(GOV_HRLD)) text += "{{R|Requires the Heraldry skill.}}\n\n";

			text += "Press the {{W|A}} and {{W|D}} keys to flip through sprites.\n\n" +
				"Hold the {{W|Shift}} or {{W|Control}} modifiers while pressing {{W|A}}, {{W|S}} or {{W|D}} to change foreground, background and detail colours." +
				"\n\nPress {{W|Control}} + {{W|R}} to reset the value.";

			Popup.Show(text);
		}

		public void Leave() {
			if (Disabled) return;
			ActiveRender.Tile = ActiveEntry.Value;
			ActiveRender.ColorString = $"&{colors[ForeIndex]}^{colors[BackIndex]}";
			ActiveRender.DetailColor = colors[DetailIndex].ToString();
		}
	}
}
