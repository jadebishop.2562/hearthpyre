using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Genkit;
using Hearthpyre.Realm;
using XRL.World.Parts.Skill;
using XRL.Rules;
using Detail = UnityEngine.Color;
using static Hearthpyre.Static;
//using Rewired;

namespace Hearthpyre.UI
{
	[UIView(ID: NAME, WantsTileOver: true, NavCategory: "Menu")]
	public class PaintView : TargetView, ManagedView
	{
		public const string NAME = "HearthpyrePaintView";
		public const int BRUSH_X = 46;

		public ColorPicker ColorPicker;
		public BrushPicker BrushPicker;
		public Brush ActiveBrush;
		public ColorBrush ColorBrush;
		//public Paint ActivePaint;

		public PaintView() : base(NAME) {
			MouseFocus.Max = 2;
			ColorPicker = new ColorPicker();
			BrushPicker = new BrushPicker();
			ColorPicker.Reload += OnColorReload;
			BrushPicker.Reload += OnBrushReload;
		}

		public override bool Showable(bool silent = false) {
			return base.Showable();
		}

		public override void LoadState() {
			BrushPicker.Enter();
			ColorPicker.Enter();
			ActiveBrush = BrushPicker.ActiveBrush;
		}

		public override void SaveState() {
			//GAME.SetIntGameState(Name + "PaintIndex", PaintIndex.Value);
			//GAME.SetStringGameState(Name + "Color", Color);
		}

		public void OnBrushReload(object obj, EventArgs args) {
			ActiveBrush = BrushPicker.ActiveBrush;
		}

		public void OnColorReload(object obj, EventArgs args) {
			BrushPicker.ColorBrush.Color = ColorPicker.ColorString;
			BrushPicker.ColorBrush.Detail = ColorPicker.DetailColor;
		}

		public override void Enter() {
			base.Enter();
		}

		public override void Render(ScreenBuffer sb) {
			var txtClr = new[] { "y", "y", "y" };
			txtClr[MouseFocus] = "C";

			sb.Y = Cursor.y < 5 ? sb.Height - 1 : 0;
			sb.HeaderLine(CLD_WHT);
			sb.Write(0, sb.Y, "<{{W|A}}");
			sb.Write(78, sb.Y, "{{W|D}}>");

			sb.Write(PEN_X, sb.Y, "[ \0{{W|Q}}-{{" + txtClr[0] + "|Pencil}} ]");
			sb[PEN_X + 2, sb.Y].Display("Icons/hp_pencil_tool.png", CLB_BLUE, CLB_CYAN);

			sb.Write(FILL_X, sb.Y, "[ \0{{W|E}}-{{" + txtClr[1] + "|Bucket}} ]");
			sb[FILL_X + 2, sb.Y].Display("Icons/hp_bucket_fill.png", CLB_BLUE, CLB_CYAN);

			sb.Write(BRUSH_X, sb.Y, "[ \0{{W|R}}-{{" + txtClr[2] + "|Brush}} ]");

			var chr = sb[BRUSH_X + 2, sb.Y];
			chr.Copy(ActiveBrush.Icon);
			if (ActiveBrush.Flags.HasFlag(Brush.ACTIVE_ICON))
				chr.Tile = ActiveObject?.pRender?.Tile ?? ActiveCell?.PaintTile;

			sb.Write(HELP_X, sb.Y, "[ \0{{W|H}}elp ]");
			sb[HELP_X + 2, sb.Y].Display("Creatures/caste_12.bmp", CLB_CYAN, CLB_WHT);

			if (ColorPicker.Visible) ColorPicker.Render(sb);
			else if (BrushPicker.Visible) BrushPicker.Render(sb);
			else base.Render(sb);
		}

		public override bool Input(Keys keys) {
			if (ColorPicker.Visible && ColorPicker.Input(keys)) return true;
			if (BrushPicker.Visible && BrushPicker.Input(keys)) return true;

			if (!base.Input(keys)) {
				switch (keys) {
					case Keys.Q:
						PencilArea();
						break;
					case Keys.E:
						FillArea();
						break;
					case Keys.R:
						DisplayBrushPicker();
						break;
					case Keys.C:
						DisplayColorPicker();
						break;
					default: return false;
				}
			}

			return true;
		}

		public void DisplayColorPicker() {
			if (ActiveBrush != BrushPicker.ColorBrush) return;

			char fg, bg, dt = BrushPicker.ColorBrush.Detail[0];
			SplitColorString(BrushPicker.ColorBrush.Color, out fg, out bg);
			ColorPicker.SetState(ActiveObject?.pRender?.Tile ?? ActiveCell?.PaintTile, fg, bg, dt);
			ColorPicker.Retranslate(BRUSH_X - 1, Cursor.y < 5 ? 25 - ColorPicker.Box.Height : 0);
			ColorPicker.Visible = true;
			MenuSound();
		}

		public void DisplayBrushPicker() {
			BrushPicker.Retranslate(BRUSH_X - 1, Cursor.y < 5 ? 25 - BrushPicker.Box.Height : 0);
			BrushPicker.Visible = true;
			MenuSound();
		}

		bool BlueprintPredicate(GameObject obj) => obj.Blueprint == ActiveObject.Blueprint;
		void FillArea() {
			if (ActiveObject != null && ActiveBrush.Flags.HasFlag(Brush.PAINTS_OBJECTS)) {
				ActiveBrush.Paint(ActiveCell.LazyCardinalFloodObjects(BlueprintPredicate));
			} else if (ActiveBrush.Flags.HasFlag(Brush.PAINTS_CELLS)) {
				ActiveBrush.Paint(ActiveCell.LazyCardinalFlood(x => !x.IsStructure()));
			}
			PlayUISound(SND_SEL);
		}

		void PencilArea() {
			if (ActiveObject != null && ActiveBrush.Flags.HasFlag(Brush.PAINTS_OBJECTS)) {
				ActiveBrush.Paint(CursorArea.SelectMany(x => x.Objects.Where(BlueprintPredicate)));
			} else if (ActiveBrush.Flags.HasFlag(Brush.PAINTS_CELLS)) {
				ActiveBrush.Paint(CursorArea);
			}
			PlayUISound(SND_SEL);
		}

		/*public void AskColorString() {
			var str = Popup.AskString("Write a color string.\nUse any combination of &&C, ^^C or *C.", Color, 6);
			if (String.IsNullOrEmpty(str)) return;
			if (str.IndexOfAny(new[] { '&', '^', '*' }) == -1)
				Popup.Show("Invalid color string.");
			else Color = Static.FilterColor(str);
		}*/

		public override void Help() {
			var text = MakeshiftPopupTitle("{{W|PAINT MODE HELP}}") + "\n" +
				"Press the {{W|Q}}, {{W|E}} or {{W|R}} keys to pencil the cursor area, bucket fill or change brushes.\n" +
				"Press the {{W|C}} key with the Color brush selected to open the color selection menu.\n\n" +
				"The {{W|A}} and {{W|D}} keys will cycle through available scheming modes.\n\n" +
				"{{W|Right Click}}: Change mouse focus.\n" +
				"{{W|Left Click}}: Use focused tool.\n" +
				"{{W|Shift}} + {{W|8}}|{{W|2}}: Change cursor size.\n" +
				"{{W|Shift}} + {{W|C}}: Change paint color.\n";

			Popup.Show(text);
		}
	}
}
