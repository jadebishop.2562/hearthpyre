using System;
using System.Linq;
using System.Collections.Generic;
using XRL;
using XRL.World;
using XRL.World.Parts.Skill;
using XRL.World.WorldBuilders;
using Genkit;

using static Hearthpyre.Static;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// A settlement, spans one parasang/tile on the world map.
	/// </summary>
	public class Settlement
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public HearthpyreGoverning Governor { get; private set; }
		public Dictionary<Guid, Sector> Sectors { get; } = new Dictionary<Guid, Sector>();
		public Dictionary<string, Sector> SectorsByZoneID { get; } = new Dictionary<string, Sector>();
		public Location Location { get; } = new Location();
		public Retainers Retainers { get; } = new Retainers();
		public string Name => Prime.Name;

		Sector prime;
		public Sector Prime {
			get {
				if (prime == null) prime = Sectors.FirstOrDefault(x => x.Value.Prime).Value;
				return prime;
			}
		}

		GameObject terrain;
		public GameObject Terrain {
			get {
				if (terrain == null || terrain.IsInvalid())
					terrain = ZoneManager.GetTerrainObjectForZone(Location.X, Location.Y, Location.World);

				return terrain;
			}
		}

		public Settlement(HearthpyreGoverning governor, SerializationReader reader) : this(governor, null, reader) { }
		public Settlement(HearthpyreGoverning governor, Zone zone = null, SerializationReader reader = null) {
			Governor = governor;
			if (zone != null) Location.Set(zone);
			if (reader != null) Load(reader);

			RealmCache.Settlements[ID] = this;

			var worldId = String.Format("{0}.{1}.{2}", Location.World, Location.X, Location.Y);
			Governor.SettlementsByWorldID[worldId] = this;
		}

		public void Save(SerializationWriter Writer) {
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			Location.SaveFields(fields);
			Retainers.SaveFields(fields);
			Writer.Write(fields);


			Writer.Write(Sectors.Count);
			foreach (var sector in Sectors.Values) {
				sector.Save(Writer);
			}
		}

		public Settlement Load(SerializationReader reader) {
			var fields = reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid)val;
			Location.LoadFields(fields);
			Retainers.LoadFields(fields);

			for (int i = 0, c = reader.ReadInt32(); i < c; i++) {
				var sector = new Sector(this, reader);
				Sectors[sector.ID] = sector;
			}
			return this;
		}

		public Sector NewSector(Zone zone) {
			var region = new Sector(this, zone);
			region.Prime = Sectors.Count == 0;
			Sectors[region.ID] = region;
			return region;
		}

		public Sector GetSector(Zone zone) { return GetSector(zone.ZoneID); }
		public Sector GetSector(string zoneId) {
			Sector result;
			SectorsByZoneID.TryGetValue(zoneId, out result);
			return result;
		}

		public void Flush() {
			prime = null;
			terrain = null;
			foreach (var sector in Sectors.Values) {
				sector.Flush();
			}
		}

		// Some clearing concerns with this, only got a loose idea of when the player loads the game.
		// Guids should be OK, ZoneID's not so much. Do for now.
		/*[HasGameBasedStaticCache]
		public static class Manager
		{
			[GameBasedStaticCache]
			public static Dictionary<Guid, Settlement> Settlements;

			[GameBasedStaticCache]
			public static Dictionary<Guid, Sector> Sector;

			[GameBasedStaticCache]
			public static Dictionary<string, Sector> SectorsByZoneID;

			[GameBasedStaticCache]
			public static Dictionary<Guid, Home> Homes;

			public static Settlement GetSettlement(Guid id) {
				Settlement result;
				Settlements.TryGetValue(id, out result);
				return result;
			}
			public static Sector GetSector(Guid id) {
				Sector result;
				Sector.TryGetValue(id, out result);
				return result;
			}
			public static Sector GetSector(string zoneId) {
				Sector result;
				SectorsByZoneID.TryGetValue(zoneId, out result);
				return result;
			}
			public static Sector GetSector(Zone zone) {
				return GetSector(zone.ZoneID);
			}
			public static Home GetHome(Guid id) {
				Home result;
				Homes.TryGetValue(id, out result);
				return result;
			}
			[Obsolete]
			public static void Clear() {
				Settlements.Clear();
				Sector.Clear();
				SectorsByZoneID.Clear();
				Homes.Clear();
			}
		}*/
	}

	// BUG: ModManager does not reflect nested classes.
	// Ahh heck these static caches're perfect, thanks gnarf
	[HasGameBasedStaticCache]
	public static class RealmCache
	{
		[GameBasedStaticCache]
		public static Dictionary<Guid, Settlement> Settlements;

		[GameBasedStaticCache]
		public static Dictionary<Guid, Sector> Sector;

		[GameBasedStaticCache]
		public static Dictionary<string, Sector> SectorsByZoneID;

		[GameBasedStaticCache]
		public static Dictionary<Guid, Home> Homes;

		public static Settlement GetSettlement(Guid id) {
			Settlement result;
			Settlements.TryGetValue(id, out result);
			return result;
		}

		public static Sector GetSector(Guid id) {
			Sector result;
			Sector.TryGetValue(id, out result);
			return result;
		}

		public static Sector GetSector(string zoneId) {
			Sector result;
			SectorsByZoneID.TryGetValue(zoneId, out result);
			return result;
		}

		public static Sector GetSector(Zone zone) {
			return GetSector(zone.ZoneID);
		}

		public static Home GetHome(Guid id) {
			Home result;
			Homes.TryGetValue(id, out result);
			return result;
		}

		[Obsolete]
		public static void Clear() {
			Settlements.Clear();
			Sector.Clear();
			SectorsByZoneID.Clear();
			Homes.Clear();
		}
	}

	public class Location
	{
		public int X { get; set; }
		public int Y { get; set; }
		public string World { get; set; }

		public void Set(Zone zone) {
			Set(zone.wX, zone.wY, zone.ZoneWorld);
		}

		public void Set(int x, int y, string world) {
			X = x;
			Y = y;
			World = world;
		}

		public void SaveFields(Dictionary<string, object> fields) {
			fields["X"] = X;
			fields["Y"] = Y;
			fields["World"] = World;
		}

		public void LoadFields(Dictionary<string, object> fields) {
			object val;
			if (fields.TryGetValue("X", out val)) X = (int)val;
			if (fields.TryGetValue("Y", out val)) Y = (int)val;
			if (fields.TryGetValue("World", out val)) World = (string)val;
		}
	}

	// TODO: Replace with straight dictionary once we can extend with generic methods.
	// TODO: Try using ?[]
	public class Retainers
	{
		Dictionary<string, string> pairs = new Dictionary<string, string>();

		public string this[string key] {
			get {
				string result;
				pairs.TryGetValue(key, out result);
				return result;
			}
			set {
				pairs[key] = value;
			}
		}

		public bool Remove(string id) {
			foreach (var retainer in pairs) {
				if (retainer.Value != id) continue;

				pairs.Remove(retainer.Key);
				return true;
			}

			return false;
		}

		public void SaveFields(Dictionary<string, object> fields) {
			foreach (var pair in pairs) fields[pair.Key] = pair.Value;
		}

		public void LoadFields(Dictionary<string, object> fields) {
			object val;
			if (fields.TryGetValue("Merchant", out val)) pairs["Merchant"] = (string)val;
			if (fields.TryGetValue("Tinker", out val)) pairs["Tinker"] = (string)val;
			if (fields.TryGetValue("Warden", out val)) pairs["Warden"] = (string)val;
		}
	}
}
