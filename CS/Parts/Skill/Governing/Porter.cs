using System;
using static Hearthpyre.Static;
using Hearthpyre;
using System.Collections.Generic;
using XRL.World.Capabilities;
using XRL.Language;
using XRL.UI;
using XRL.Rules;
using Genkit;
using System.Linq;
using XRL.World.Effects;

namespace XRL.World.Parts.Skill
{
	[Serializable]
	public class HearthpyreGoverning_Porter : BaseSkill
	{
		public const string ABL_NAME = "Cart Blitz [{{W|attack}}]";
		public const string ABL_CMD = "CommandPorterCharge";
		public const string ABL_CAT = "Skill";
		public const string ABL_DESC = "You charge forward 3 to 5 squares and make a splash attack based on your encumbrance.\n\nThe defenders must succeed an agility saving throw or be knocked prone.\n\nRequires a cart.";
		public const string SAVE_ID = GOV_PRT + "SaveVersion";
		public const int ABL_MIN = 3;
		public const int ABL_MAX = 5;
		public const int SAVE_VER = 2;

		// Ah piss, oh bugger. Forgot to serialise this. FOOL!
		Guid AbilityID;

		public override void SaveData(SerializationWriter writer) {
			base.SaveData(writer);

			var fields = new Dictionary<string, object>();
			fields["Ability"] = AbilityID;
			writer.Write(fields);
		}

		public override void LoadData(SerializationReader reader) {
			base.LoadData(reader);

			if (GAME.GetIntGameState(SAVE_ID) < SAVE_VER) {
				try {
					UpgradeSaveVersion();
				} catch (Exception e) { Static.Log("Error upgrading save version", e); }
				return;
			}

			var fields = reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid)val;
		}

		// TODO: Remove once Qud breaks saves.
		void UpgradeSaveVersion() {
			GAME.SetIntGameState(SAVE_ID, SAVE_VER);

			var abilities = ParentObject.ActivatedAbilities;
			if (abilities == null) return;

			var ability = abilities.GetAbility(ABL_CMD);
			if (ability != null) {
				abilities.RemoveAbility(ability.ID);
				AbilityID = AddMyActivatedAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC, Silent: true);
			}
		}

		public override void Register(GameObject obj) {
			obj.RegisterPartEvent(this, ABL_CMD);
			obj.RegisterPartEvent(this, "AIGetOffensiveMutationList");
			base.Register(obj);
		}

		public override bool FireEvent(Event E) {
			if (E.ID == ABL_CMD) {
				AttemptCharge();
			} else if (E.ID == "AIGetOffensiveMutationList") {
				AIChargeOption(E);
			}
			return base.FireEvent(E);
		}

		public override bool AddSkill(GameObject obj) {
			GAME.SetIntGameState(SAVE_ID, SAVE_VER);
			AbilityID = AddMyActivatedAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC);
			obj.UpdateEquipped(PRT_CART);
			return true;
		}

		public override bool RemoveSkill(GameObject obj) {
			RemoveMyActivatedAbility(ref AbilityID);
			obj.UpdateEquipped(PRT_CART);
			return true;
		}

		void AIChargeOption(Event E) {
			if (!CanCharge()) return;

			var dist = E.GetIntParameter("Distance");
			var target = E.GetGameObjectParameter("Target");
			if (dist <= ABL_MIN || dist > ABL_MAX + 1) return;
			if (!IsMyActivatedAbilityUsable(AbilityID)) return;
			if (!target.FlightMatches(ParentObject)) return;

			E.AddAICommand(ABL_CMD);
		}

		bool CanCharge() {
			if (!ParentObject.LazyForeachEquip(x => x.HasPart("HearthpyreCart")).Any())
				ParentObject.PlayerPopup("You cannot charge without a cart.");
			else if (ParentObject.OnWorldMap())
				ParentObject.PlayerPopup("You cannot charge on the world map.");
			else if (Flight.IsFlying(ParentObject))
				ParentObject.PlayerPopup("You cannot charge while flying.");
			else if (ParentObject.AreHostilesAdjacent())
				ParentObject.PlayerPopup("You cannot charge while engaged in melee.");
			else
				return true;

			return false;
		}

		void AttemptCharge() {
			if (!CanCharge()) return;

			var line = PickLine(ABL_MAX + 1, AllowVis.OnlyVisible, x => x != null && x.IsCombatObject() && x.FlightMatches(ParentObject));
			if (line == null) return;
			if (line.Count - 2 < ABL_MIN) {
				ParentObject.PlayerPopup($"You must charge at least {Grammar.Cardinal(ABL_MIN)} spaces.");
				return;
			}
			if (line.Count - 2 > ABL_MAX) {
				ParentObject.PlayerPopup($"You can't charge more than {Grammar.Cardinal(ABL_MAX)} spaces.");
				return;
			}

			var cell = line.Last();
			var target = ParentObject.Target;
			if (IsPlayer()) target = cell.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
			if (target == null) {
				ParentObject.PlayerPopup("You must charge at a target!");
				return;
			}

			Charge(line);
		}

		void Charge(List<Cell> line) {
			var inventory = ParentObject.GetPart<Inventory>();
			var steps = new string[line.Count - 1];
			for (int i = 0; i < line.Count - 1; i++)
				steps[i] = line[i].GetDirectionFromCell(line[i + 1]);

			for (int i = 0, c = steps.Length + ABL_MIN; i < c; i++) {
				var step = steps[i % steps.Length];
				var cell = ParentObject.CurrentCell.GetCellFromDirection(step);
				var target = cell.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
				if (target != null) {
					DidXToY("crash", "right into", target, terminalPunctuation: "!", ColorAsBadFor: target);
					PlayWorldSound("Hit_Default", 0.5f, 0.2f);
					cell.ForeachLocalAdjacentCellAndSelf(x => {
						var hostile = x.GetCombatTarget(ParentObject, InanimateSolidOnly: true);
						if (hostile == null || hostile == ParentObject) return;

						Damage(hostile);
						ParentObject.FireEvent(Event.New("ChargedTarget", "Defender", hostile));
						hostile.FireEvent(Event.New("WasCharged", "Attacker", ParentObject));
					});
					break;
				} else {
					ParentObject.TileParticleBlip("Items/hp_cart.png", "&K", "y", 10 + i * 5);
					if (!ParentObject.Move(step, EnergyCost: 0))
						break;
				}
			}
			ParentObject.UseEnergy(1000, "Charging");
			CooldownMyActivatedAbility(AbilityID, 40);
		}

		void Damage(GameObject target) {
			var die = Math.Max(1, RoundToInt(ParentObject.GetTotalWeight() / 100d));
			var dmg = new Damage(Stat.Roll(die + "d4+" + ParentObject.StatMod("Strength") * 2));
			var take = Event.New("TakeDamage", 0, 0, 0);
			take.AddParameter("Damage", dmg);
			take.AddParameter("Owner", ParentObject);
			take.AddParameter("Attacker", ParentObject);
			take.AddParameter("Message", "from %o cart blitz!");
			target.FireEvent(take);

			if (!target.MakeSave("Agility", die * 2, ParentObject, "Strength", "Cart Blitz Knockdown"))
				target.ApplyEffect(new Prone());
		}
	}
}
