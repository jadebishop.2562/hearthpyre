using System;
using System.Collections.Generic;
using XRL.Rules;
using XRL.UI;
using static Hearthpyre.Static;
using Hearthpyre;

namespace XRL.World.Parts.Skill
{
	[Serializable]
	public class HearthpyreGoverning_Xylogrifter : BaseSkill
	{
		public override bool AddSkill(GameObject obj) {
			obj.UpdateEquipped(PRT_SCMR);
			return true;
		}

		public override bool RemoveSkill(GameObject obj) {
			obj.UpdateEquipped(PRT_SCMR);
			return true;
		}
	}
}
