using System;
using System.Collections.Generic;
using System.Linq;
using Hearthpyre;
using Hearthpyre.UI;
using Hearthpyre.Realm;
using Hearthpyre.Dialogue;
using XRL.Core;
using XRL.UI;
using XRL.Rules;
using XRL.World.WorldBuilders;
using static Hearthpyre.Static;

namespace XRL.World.Parts.Skill
{
	[Serializable]
	public class HearthpyreGoverning : BaseSkill
	{
		public const string ABL_NAME = "Govern";
		public const string ABL_CMD = "CommandGovern";
		public const string ABL_CAT = "Skill";
		public const string ABL_DESC = "Stake or manage a claim in the world.";

		Guid AbilityID = Guid.Empty;
		// TODO: Consider moving village data to gamestate singleton.
		public Dictionary<Guid, Settlement> Settlements { get; private set; } = new Dictionary<Guid, Settlement>();
		public Dictionary<string, Settlement> SettlementsByWorldID { get; private set; } = new Dictionary<string, Settlement>();

		WorldInfo world;
		public WorldInfo World {
			get {
				if (world == null)
					world = GAME.GetObjectGameState("JoppaWorldInfo") as WorldInfo;

				return world;
			}
		}

		public override void SaveData(SerializationWriter writer) {
			base.SaveData(writer);

			var fields = new Dictionary<string, object>();
			fields["Ability"] = AbilityID;
			writer.Write(fields);

			writer.Write(Settlements.Count);
			foreach (var settlement in Settlements) {
				settlement.Value.Save(writer);
			}
		}

		public override void LoadData(SerializationReader reader) {
			base.LoadData(reader);

			var fields = reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid)val;

			for (int i = 0, c = reader.ReadInt32(); i < c; i++) {
				var settlement = new Settlement(this, reader);
				Settlements[settlement.ID] = settlement;
			}
		}

		public override bool AllowStaticRegistration() => true;
		public override void Register(GameObject Object) {
			Object.RegisterPartEvent(this, ABL_CMD);
			base.Register(Object);
		}

		public override bool WantEvent(int ID, int cascade) {
			return 
				base.WantEvent(ID, cascade) 
				|| ID == BeginConversationEvent.ID
			;
		}

		public override bool HandleEvent(BeginConversationEvent E) {
			Dialogue.Speaker = E.SpeakingWith;
			if (E.Conversation == null || E.SpeakingWith == null || !Settlements.Any()) return true;

			AddChoice(E.Conversation, E.SpeakingWith);
			return true;
		}

		public override bool FireEvent(Event E) {
			if (E.ID == ABL_CMD) {
				View.Manager.Show(GovernView.NAME);
			}
			return base.FireEvent(E);
		}

		public override bool AddSkill(GameObject GO) {
			AbilityID = AddMyActivatedAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC);
			return true;
		}

		public override bool RemoveSkill(GameObject GO) {
			RemoveMyActivatedAbility(ref AbilityID);
			return true;
		}

		public bool SpeakerPredicate(GameObject obj) {
			if (obj.HasTagOrProperty("ExcludeFromDynamicEncounters")) return false;
			if (obj.pBrain == null) return false;

			return true;
		}

		public void AddChoice(Conversation conv, GameObject speaker) {
			if (!SpeakerPredicate(speaker)) return;

			var start = conv.NodesByID["Start"];
			if (!start.bCloseable) return;

			foreach (var pair in DialogueLoader.Dialogues["Hearthpyre"].NodesByID) {
				conv.AddNode(pair.Value);
			}

			var choice = conv.NodesByID["InviteStart"].Choices[0];
			if (speaker.HasPart(PRT_STLR))
				choice = conv.NodesByID["ManageStart"].Choices[0];
			choice.Ordinal = 1010;
			start.Choices.Add(choice);
		}

		public Settlement NewSettlement(Zone zone) {
			var settlement = new Settlement(this, zone);
			Settlements[settlement.ID] = settlement;
			return settlement;
		}

		public Settlement GetSettlement(Zone zone) => GetSettlement(zone.wX, zone.wY, zone.ZoneWorld);

		public Settlement GetSettlement(int wX, int wY, string world = "JoppaWorld") {
			var worldId = Strings.SB.Clear().Append(world).Append('.')
				.Append(wX).Append('.')
				.Append(wY).ToString();

			return GetSettlement(worldId);
		}

		public Settlement GetSettlement(string worldId) {
			Settlement result;
			SettlementsByWorldID.TryGetValue(worldId, out result);
			return result;
		}

		public void Flush() {
			//foreach (var village in Villages.Values) {
			foreach (var pair in Settlements) {
				var settlement = pair.Value;
				settlement.Flush();
			}
		}
	}
}
