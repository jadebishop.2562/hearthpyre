﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using XRL.UI;
using XRL.Core;
using XRL.Rules;
using Hearthpyre;
using Hearthpyre.UI;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public partial class HearthpyreXyloschemer : IPoweredPart, IXmlSerializable
	{
		// TODO: Make this less garbage.
		// TODO: add some funky charge-consuming on hit effect
		public static HearthpyreXyloschemer ACTIVE;

		public const string ABL_NAME = "Activate Xyloschemer";
		public const string ABL_CMD = "ActivateXyloschemer";
		public const string ABL_CAT = "Items";
		public const string ABL_DESC = "Activate a charged xyloschemer.";
		public const string INV_CMD = "InvCommandScheme";

		Guid AbilityID;
		int FrameOffset;

		public List<string> Packages { get; set; } = new List<string>();

		public override bool SameAs(IPart p) {
			return false;
		}

		public override bool Render(RenderEvent E) {
			var f = (XRLCore.CurrentFrame + FrameOffset) & 511;
			if (f < 8) E.DetailColor = "C";
			else if (f < 16) E.DetailColor = "c";

			FrameOffset += Stat.RandomCosmetic(0, 24);
			if (Stat.RandomCosmetic(1, 400) == 1) {
				E.DetailColor = "Y";
				E.ColorString = "&Y";
			}

			return true;
		}

		#region Serialization
		public override void SaveData(SerializationWriter writer) {
			base.SaveData(writer);

			var fields = new Dictionary<string, object>();
			fields["Packages"] = Packages.ToArray();
			fields["Ability"] = AbilityID;
			writer.Write(fields);
		}

		public override void LoadData(SerializationReader reader) {
			base.LoadData(reader);

			var fields = reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Packages", out val)) Packages.AddRange((string[])val);
			if (fields.TryGetValue("Ability", out val)) AbilityID = (Guid)val;
		}


		public XmlSchema GetSchema() => null;
		public void WriteXml(XmlWriter writer) {
			foreach (var package in Packages) {
				writer.WriteStartElement("Package");
				writer.WriteAttributeString("Name", package);
				writer.WriteEndElement();
			}
		}

		public void ReadXml(XmlReader reader) {
			var depth = reader.Depth;
			reader.ReadStartElement();
			while (reader.Read() && reader.Depth > depth) {
				if (reader.IsStartElement("Package")) {
					Packages.Add(reader.GetAttribute("Name"));
				}
			}
			reader.ReadEndElement();
		}

		#endregion

		#region Events
		//public override bool AllowStaticRegistration() => true;

		public override void Register(GameObject obj) {
			obj.RegisterPartEvent(this, "ExamineSuccess");
			obj.DisplayName = "{{shimmering b-b-B sequence|xyloschemer}}";
		}

		public override bool FireEvent(Event E) {
			if (E.ID == ABL_CMD) {
				// TODO: Merge with InventoryActionEvent when Abilities fire MinEvents.
				var obj = ParentObject.GetBearer();
				if (obj == null || !obj.IsPlayer() || !UseCharge(obj, 25, false)) return true;

				ACTIVE = this;
				E.RequestInterfaceExit();
				GameManager.Instance.gameQueue.queueTask(() => View.Manager.Show(SchemeView.NAME));
				return false;
			} else if (E.ID == "ExamineSuccess") {
				var obj = ParentObject.GetBearer();
				if (obj == null) return true;

				AddAbility(obj);
			}
			return base.FireEvent(E);
		}

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == GetInventoryActionsEvent.ID ||
				ID == InventoryActionEvent.ID || ID == EquippedEvent.ID ||
				ID == UnequippedEvent.ID || ID == TakenEvent.ID || ID == DroppedEvent.ID;
		}

		public override bool HandleEvent(GetInventoryActionsEvent E) {
			E.AddAction(Name: "Scheme", Display: "{{W|s}}cheme", Command: INV_CMD, Key: 's');
			return true;
		}

		public override bool HandleEvent(EquippedEvent E) {
			AdjustStats(E.Actor.HasSkill(GOV_GRFT));
			AddAbility(E.Actor);
			return true;
		}

		public override bool HandleEvent(UnequippedEvent E) {
			AdjustStats(false);
			return true;
		}

		public override bool HandleEvent(TakenEvent E) {
			AddAbility(E.Actor);
			return true;
		}

		public override bool HandleEvent(DroppedEvent E) {
			RemoveAbility(E.Actor);
			return true;
		}

		public override bool HandleEvent(InventoryActionEvent E) {
			if (E.Command != INV_CMD) return true;
			if (!E.Actor.IsPlayer() || !UseCharge(E.Actor, 25)) return true;

			ACTIVE = this;
			E.RequestInterfaceExit();
			GameManager.Instance.gameQueue.queueTask(() => View.Manager.Show(SchemeView.NAME));
			return false;
		}
		#endregion

		/// <summary>
		/// Adjust melee weapon stats for xylogrifters.
		/// </summary>
		void AdjustStats(bool state) {
			var weapon = ParentObject.GetPart<MeleeWeapon>();
			if (state) {
				weapon.BaseDamage = "2d4";
				weapon.MaxStrengthBonus = 5;
			} else {
				var blueprint = ParentObject.GetBlueprint();
				weapon.BaseDamage = blueprint.GetPartParameter("MeleeWeapon", "BaseDamage", "1d2");
				weapon.MaxStrengthBonus = int.Parse(blueprint.GetPartParameter("MeleeWeapon", "MaxStrengthBonus", "0"));
			}
		}

		public bool UseCharge(GameObject user, int charge, bool silent = false) {
			if (user.HasSkill(GOV_GRFT)) {
				charge = RoundToInt(charge * 0.75);
			}
			if (!ParentObject.UseCharge(charge, false)) {
				if (user != null && !silent)
					user.PlayerMessage("&R{0}{1}&R fuzzes and the holographic display shifts crimson. Out of juice.", ParentObject.The, ParentObject.DisplayNameOnly);
				return false;
			}
			return true;
		}


		void AddAbility(GameObject obj) {
			if (!ParentObject.Understood()) return;
			obj.RegisterPartEvent(this, ABL_CMD);

			var abilities = obj.GetPart<ActivatedAbilities>();
			if (abilities == null || AbilityID != Guid.Empty) return;

			var ability = abilities.GetAbility(ABL_CMD);
			if (ability != null) {
				AbilityID = ability.ID;
				return;
			}

			AbilityID = abilities.AddAbility(Name: ABL_NAME, Command: ABL_CMD, Class: ABL_CAT, Description: ABL_DESC, Silent: true);
		}

		void RemoveAbility(GameObject obj) {
			obj.UnregisterPartEvent(this, ABL_CMD);
			if (obj.LazyForeachItems(x => x.Blueprint == ParentObject.Blueprint).Any())
				return;

			var abilities = obj.GetPart<ActivatedAbilities>();
			if (abilities == null) return;
			if (AbilityID != Guid.Empty) abilities.RemoveAbility(AbilityID);

			var ability = abilities.GetAbility(ABL_CMD);
			if (ability != null) abilities.RemoveAbility(ability.ID);

			AbilityID = Guid.Empty;
		}
	}
}
