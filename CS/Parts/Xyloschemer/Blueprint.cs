﻿using System;
using System.Linq;
using System.Collections.Generic;

using Genkit;
using XRL.UI;
using XRL.Core;
using XRL.Rules;
using Hearthpyre;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreBlueprint : IPart
	{
		public const string INV_BLD = "InvCommandBuild";
		public const string INV_BLD_ALL = INV_BLD + "All";

		public string Blueprint { get; set; }
		int Cost = 100;

		int FrameOffset;

		public override bool SameAs(IPart p) {
			var bp = p as HearthpyreBlueprint;
			if (bp != null)
				return bp.Blueprint == Blueprint;
			return false;
		}

		public override void SaveData(SerializationWriter Writer) {
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields["Blueprint"] = Blueprint;
			fields["Cost"] = Cost;
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader) {
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;
			if (fields.TryGetValue("Blueprint", out val)) Blueprint = (string)val;
			if (fields.TryGetValue("Cost", out val)) Cost = (int)val;
		}

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == GetInventoryActionsEvent.ID ||
				ID == CanSmartUseEvent.ID || ID == CommandSmartUseEvent.ID ||
				ID == ForceApplyEffectEvent.ID || ID == InventoryActionEvent.ID;
		}

		public override bool HandleEvent(GetInventoryActionsEvent E) {
			E.AddAction(Name: "Build", Display: "{{W|b}}uild", Command: INV_BLD, Key: 'b');
			E.AddAction(Name: "BuildAll", Display: "build {{W|a}}ll", Command: INV_BLD_ALL, Key: 'a');
			return true;
		}

		public override bool HandleEvent(InventoryActionEvent E) {
			if (E.Command == INV_BLD) {
				Build(E.Actor);
			} else if (E.Command == INV_BLD_ALL) {
				var C = ParentObject.CurrentCell;
				if (!Build(E.Actor, true)) return true;

				XRLCore.Core.RenderDelay(300);

				foreach (var obj in C.LazyCardinalObjects(PRT_BLPR)) {
					var part = obj.GetPart(PRT_BLPR) as HearthpyreBlueprint;
					if (part.SameAs(this)) { part.HandleEvent(E); }
				}
			}
			return true;
		}

		public override bool HandleEvent(CanSmartUseEvent E) {
			// TODO: Find out why objects do not get created when player occupies cell.
			// QUIRK: Returned true means no.
			return E.Actor.CurrentCell == ParentObject.CurrentCell;
		}

		public override bool HandleEvent(CommandSmartUseEvent E) {
			Build(E.Actor);
			return true;
		}

		public override bool HandleEvent(ForceApplyEffectEvent E) {
			return false;
		}

		// Toned down holo-effect, having tons of standard holo-objects was straining to both eye and computer.
		public override bool Render(RenderEvent E) {
			int num = (XRLCore.CurrentFrame + FrameOffset) & 2047;
			if (num < 16)
				E.ColorString = "&C";
			else if (num < 32)
				E.ColorString = "&c";

			FrameOffset += Stat.RandomCosmetic(0, 48);
			return true;
		}

		public GameObject GetXyloschemer(GameObject actor) {
			return actor.LazyForeachItems((item) => item.Blueprint == OBJ_SCMR).FirstOrDefault();
		}

		public bool Build(GameObject actor, bool silent = false) {
			var xyloschemer = GetXyloschemer(actor);
			if (xyloschemer == null) {
				xyloschemer = GameObjectFactory.Factory.CreateSampleObject(OBJ_SCMR);
				if (!silent) actor.PlayerMessage("You can't build that without {0}{1}.", xyloschemer.a, xyloschemer.DisplayNameOnly);
				xyloschemer.Destroy();
				return false;
			}

			var part = xyloschemer.GetPart(PRT_SCMR) as HearthpyreXyloschemer;
			if (part == null || !part.UseCharge(actor, Cost))
				return false;

			var C = ParentObject.CurrentCell;
			HoloZap(this, C);
			C.RemoveObject(ParentObject);
			ParentObject.Destroy();

			var obj = C.Construct(Blueprint);
			if (obj == null) return false;

			if (!silent) actor.PlayerMessage("You zap {0}{1} into existance with {2}{3}.", obj.a, obj.DisplayName, xyloschemer.the, xyloschemer.DisplayNameOnly);
			return true;
		}

		public void SetDesign(GameObjectBlueprint design) {
			var render = design.GetPart("Render");
			if (render != null) {
				string param;
				if (render.Parameters.TryGetValue("DisplayName", out param))
					ParentObject.pRender.DisplayName = param + " {{b-b-B sequence|blueprint}}";
				if (render.Parameters.TryGetValue("Tile", out param))
					ParentObject.pRender.Tile = param;
				if (render.Parameters.TryGetValue("RenderString", out param))
					ParentObject.pRender.RenderString = Static.RenderStrToChar(param);
				if (render.Parameters.TryGetValue("RenderLayer", out param))
					ParentObject.pRender.RenderLayer = Calc.Clamp(Convert.ToInt32(param), 3, 7) * 10;
				if (render.Parameters.TryGetValue("DetailColor", out param))
					ParentObject.pRender.DetailColor = param == "k" ? "k" : "K";
			}

			var tile = design.GetPart("PickRandomTile");
			if (tile != null) {
				var part = new PickRandomTile {
					ParentObject = ParentObject,
					Tile = tile.GetParameter("Tile")
				};
				//tile.InitializePartInstance(part); // Hmm, this caused tile loading issues for both blueprint and design.
				ParentObject.AddPart(part);

				if (part.WantEvent(ObjectCreatedEvent.ID, ObjectCreatedEvent.CascadeLevel))
					part.HandleEvent(ObjectCreatedEvent.FromPool(ParentObject));
				else
					part.FireEvent(Event.New("ObjectCreated"));
			}

			var nabables = new[] { "PaintedWall", "PaintedWallAtlas", "PaintedWallExtension",
				"PaintedFence", "PaintedFenceAtlas", "PaintedFenceExtension", "PaintPart", "PaintWith" };
			foreach (var key in nabables) {
				if (design.Tags.ContainsKey(key))
					ParentObject.SetStringProperty(key, design.Tags[key]);
			}

			Blueprint = design.Name;
			Cost = (1 + Calc.Clamp(design.Tier, 0, 10)) * 50;
		}
	}
}
