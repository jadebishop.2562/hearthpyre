using System;

using XRL.UI;
using XRL.Rules;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.Names;
using ConsoleLib.Console;
using XRL.Core;
using Hearthpyre;
using Hearthpyre.Realm;
using System.Collections.Generic;
using System.Linq;
using Hearthpyre.AI;
using HeroMaker = XRL.World.ZoneBuilders.HeroMaker;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreSettler : IPart
	{
		Settlement settlement;
		public Guid SettlementID { get; private set; }
		public Settlement Settlement {
			get {
				if (settlement == null && SettlementID != Guid.Empty)
					settlement = RealmCache.GetSettlement(SettlementID);

				return settlement;
			}
			set {
				settlement = value;
				SettlementID = value != null ? value.ID : Guid.Empty;
			}
		}

		public Sector CurrentSector => Settlement.GetSector(currentCell.ParentZone.ZoneID);

		Home home;
		public Guid HomeID { get; private set; }
		public Home Home {
			get {
				// Check if home has been removed.
				if (home != null && home.Sector == null) {
					if (home.ID == HomeID) HomeID = Guid.Empty;
					home = null;
				}

				if (home == null && HomeID != Guid.Empty)
					home = RealmCache.GetHome(HomeID);

				return home;
			}
			set {
				home = value;
				HomeID = value != null ? value.ID : Guid.Empty;
			}
		}

		public override bool SameAs(IPart p) {
			var settler = p as HearthpyreSettler;
			return settler != null && settler.HomeID == HomeID;
		}

		public override void SaveData(SerializationWriter Writer) {
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			if (SettlementID != Guid.Empty)
				fields.Add("Village", SettlementID);
			if (HomeID != Guid.Empty)
				fields.Add("Home", HomeID);
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader) {
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("Village", out val)) SettlementID = (Guid)val;
			if (fields.TryGetValue("Home", out val)) HomeID = (Guid)val;
		}

		public override void Register(GameObject obj) {
			InitializeSettler(obj);
			obj.RegisterPartEvent(this, "AIBored");
			base.Register(obj);
		}

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == BeforeDestroyObjectEvent.ID;
		}

		public override bool HandleEvent(BeforeDestroyObjectEvent E) {
			Settlement.Retainers.Remove(E.Object.id);
			return true;
		}

		public override bool FireEvent(Event E) {
			if (E.ID == "AIBored") {
				ParentObject.pBrain.PushGoal(new SedentaryLiving());
				return false;
			}
			return base.FireEvent(E);
		}

		void InitializeSettler(GameObject obj) {
			if (obj.GetIntProperty("NamedVillager") <= 0) {
				if (!obj.HasProperName) {
					obj.pRender.DisplayName = NameMaker.MakeName(For: obj);
					obj.SetIntProperty("ProperNoun", 1);
					obj.SetIntProperty("Renamed", 1);
				}
				var brain = ParentObject.pBrain;
				var faction = brain.GetPrimaryFaction();
				if (brain.FactionMembership.TryGetValue(faction, out var membership) && membership <= 75) {
					brain.FactionMembership[faction] = 100;
				}
				brain.Goals.Clear();
				brain.PartyLeader = null;
				brain.StartingCell = null;
				brain.Hostile = false;
				brain.Wanders = true;
				brain.WandersRandomly = false;
				brain.setFactionMembership(FAC_STLR, 75);
				obj.SetIntProperty("NamedVillager", 1);
			}
		}
	}
}
