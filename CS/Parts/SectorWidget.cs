using Hearthpyre.Realm;

namespace XRL.World.Parts
{
	// TODO: Add random events like visiting travelers.
	// How to implement IZ and OOZ?
	// Extensible through attributes?
	public class HearthpyreSectorWidget : IPart
	{
		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == SuspendingEvent.ID;
		}

		public override bool HandleEvent(SuspendingEvent E) {
			var id = ParentObject.CurrentZone?.ZoneID;
			if (id != null && RealmCache.SectorsByZoneID.TryGetValue(id, out var sector)) {
				sector.Flush();
			}

			return base.HandleEvent(E);
		}

		//public override bool WantTurnTick() => true;

		/*public override void TurnTick(long TurnNumber) {
			var Active = GAME.ZoneManager.ActiveZone;
			var Current = ParentObject.CurrentZone;
			if (Active.wX != Current.wX || Active.wY != Current.wY)
				return;

			ParentObject.CurrentZone.LastActive = GAME.Turns;
		}*/
	}
}