using System;
using System.Collections.Generic;
using Hearthpyre;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyrePairedStairs : IPart
	{
		public const string NAME = "HearthpyrePairedStairs";

		public bool StairsUp => ParentObject.HasPart("StairsUp");
		public bool StairsDown => ParentObject.HasPart("StairsDown");

		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == EnterCellEvent.ID || ID == LeavingCellEvent.ID;
		}

		public override bool HandleEvent(EnterCellEvent E) {
			var zone = E.Cell.ParentZone;
			// BUG: ZoneConnectionCache null for deserialised zones. Causes error in StairsUp/Down parts when trying to cache.
			if (zone.ZoneConnectionCache == null) zone.ZoneConnectionCache = new List<CachedZoneConnection>();
			if (!zone.Built) return true;

			if (StairsUp) {
				var c = E.Cell.Above();
				c.ClearObjectsWithTag("Wall");
				if (!c.HasObjectWithPart(NAME)) c.Construct(GetComplement());
			} else if (StairsDown) {
				var c = E.Cell.Below();
				c.ClearObjectsWithTag("Wall");
				if (!c.HasObjectWithPart(NAME)) c.Construct(GetComplement());
			}
			return true;
		}

		public override bool HandleEvent(LeavingCellEvent E) {
			if (StairsUp) {
				DemolishPaired(E.Cell.Above());
			} else if (StairsDown) {
				DemolishPaired(E.Cell.Below());
			}
			return true;
		}

		public void DemolishPaired(Cell cell) {
			var paired = cell.GetFirstObjectWithPart(NAME);
			if (paired == null) return;

			paired.RemovePart(NAME);
			cell.Demolish(paired);
		}

		public string GetComplement() {
			var name = ParentObject.Blueprint;
			var result = "HearthpyreStairsDown";

			if (StairsUp) {
				var i = name.LastIndexOf("Up");
				if (i > 0) result = name.Substring(0, i) + "Down";
			} else {
				var i = name.LastIndexOf("Down");
				if (i > 0) result = name.Substring(0, i) + "Up";
				else result = "HearthpyreStairsUp";
			}

			return result;
		}
	}
}
