using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.UI;
using XRL.Messages;

using static Hearthpyre.Static;

namespace Hearthpyre
{
	public static class Wishes
	{
		[XRL.Wish.WishCommand(Command = "hearthpyre:corral")]
		public static void CorralVillagers() {
			Loading.LoadTask("Corraling lost settlers", () => {
				foreach (var zone in LoopAllZones()) {
					try { CorralZone(zone); } catch (Exception e) {
						XRLCore.LogError("Hearthpyre:Corral", e);
					}
				}
			});
		}

		[XRL.Wish.WishCommand(Command = "debug:build")]
		public static void BuildZones(string strAmount) {
			var amount = Int32.Parse(strAmount);
			var manager = GAME.ZoneManager;
			var random = new Random();
			for (int i = 0; i < amount; i++) {
				var zone = manager.GetZone("JoppaWorld", random.Next(79), random.Next(24), random.Next(2), random.Next(2), random.Next(10, 30));
				if (manager.SuspendZone(zone)) ZoneManager.FreezeZone(zone);
				else MessageQueue.AddPlayerMessage("Failed suspending " + zone.ZoneID);
				Event.ResetPool();
				GC.Collect();
			}
		}

		public static void CorralZone(Zone zone) {
			foreach (var settler in zone.LoopObjectsWithPart("HearthpyreSettler")) {
				var part = settler.GetPart<HearthpyreSettler>();
				if (part.CurrentSector == null) {
					var cell = part.Settlement.Prime.Zone.GetEmptyReachableCells().GetRandomElement();
					settler.TeleportTo(cell);
					MessageQueue.AddPlayerMessage(settler.The + settler.DisplayName + " found a way home!", 'G');
				}
			}
		}

		public static IEnumerable<Zone> LoopAllZones() {
			var manager = GAME.ZoneManager;
			foreach (var pair in manager.CachedZones) {
				yield return pair.Value;
			}

			var FrozenZones = (typeof(ZoneManager).GetField("FrozenZones", BindingFlags.NonPublic | BindingFlags.Static).GetValue(null) as List<string>).ToList();
			foreach (var id in FrozenZones) {
				var zone = manager.GetZone(id);
				yield return zone;
				if (manager.SuspendZone(zone)) ZoneManager.FreezeZone(zone);
				else MessageQueue.AddPlayerMessage("Failed suspending " + zone.ZoneID);
				Event.ResetPool();
				GC.Collect();
			}
		}
	}
}
