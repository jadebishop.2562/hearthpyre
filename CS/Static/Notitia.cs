using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using XRL;
using XRL.UI;
using XRL.World;
using XRL.Language;
using SimpleJSON;
using ConsoleLib.Console;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	public static class Notitia
	{
		public static Dictionary<string, Category> Categories = new Dictionary<string, Category>();
		public static List<Entry> Icons = new List<Entry>();
		public static List<Entry> Music = new List<Entry>();

		public static void Load() {
			Loading.LoadTask("Loading Hearthpyre.json", Init);
		}

		public static void Init() {
			if (Categories.Count > 0) return;

			// Load hearthpyre data from this and other mods.
			ModManager.ForEachFile("Hearthpyre.json", (file, mod) => {
				if (!mod.IsApproved) return;
				Log($"Loading Hearthpyre.json from {mod.ID}...");

				var data = JSON.Parse(File.ReadAllText(file)) as JSONClass;
				LoadCategories(data, "BLUEPRINTS");
				LoadEntries(data, "ICONS", Icons);
				LoadEntries(data, "MUSIC", Music);
			});
		}

		static void LoadEntries(JSONClass json, string key, List<Entry> list) {
			if (json[key] == null) return;

			foreach (JSONClass obj in json[key].AsArray) {
				list.Add(new Entry(obj["Name"], obj["Value"], obj["Detail"]));
			}
			list.Sort((x, y) => String.Compare(x.Name, y.Name));
		}

		public static void LoadCategories(JSONClass json, string key) {
			var blueprints = json[key];
			if (blueprints == null) return;

			foreach (KeyValuePair<string, JSONNode> pair in blueprints.AsObject) {
				var name = pair.Key; var node = pair.Value;

				Category category;
				if (!Categories.TryGetValue(name, out category))
					category = new Category(node);

				var items = pair.Value["Items"].AsArray;
				foreach (JSONNode item in items) {
					GameObjectBlueprint blueprint;
					if (GameObjectFactory.Factory.Blueprints.TryGetValue(item["Value"], out blueprint))
						category.AllBlueprints.Add(new Blueprint(item, blueprint));
				}

				if (category.AllBlueprints.Count < 1) continue;
				category.AllBlueprints.Sort((x, y) => String.Compare(x.Name, y.Name));
				Categories[name] = category;
			}

		}

		public class Blueprint
		{
			public string Name;
			public string Package;
			public string Author;

			public GameObjectBlueprint Value;
			public ConsoleChar Icon;


			public Blueprint(string name, GameObjectBlueprint value, string package = null, string author = null) {
				Name = name.Truncate(13, ".");
				Value = value;
				Package = package;
				Author = author;
				Icon = new ConsoleChar { Foreground = CLD_BLK, Background = CLD_BLK, Detail = CLD_BLK };

				Icon.Imitate(value);
			}

			public Blueprint(JSONNode node, GameObjectBlueprint value) : this(
				node["Name"],
				value,
				node["Package"],
				node["Author"]
			) { }
		}

		public class Entry
		{
			public string Name;
			public string Value;
			public string Detail;
			// Ain't enough available tracks/icons to warrant gating them.
			//public string Accomplishment;

			public Entry(string name, string value, string detail = null) {
				Name = name;
				Value = value;
				Detail = detail;
			}
		}

		public class Category
		{
			public string Name;
			public List<Blueprint> AllBlueprints;
			public List<Blueprint> Blueprints;
			public ConsoleChar Icon;

			private int Hash;

			public Category(string name, string icon = null, string color = null, string detail = null) {
				Name = name.Truncate(5, ".");
				AllBlueprints = new List<Blueprint>();
				Icon = new ConsoleChar { Foreground = CLD_BLK, Background = CLD_BLK, Detail = CLD_BLK };

				if (icon != null) Icon.Tile = icon;
				if (color != null) Icon.SetColorString(color);
				if (detail != null) Icon.Detail = ColorUtility.ColorMap[detail[0]];
			}

			public Category(JSONNode node) : this(
				node.Key,
				node["Icon"],
				node["Color"],
				node["Detail"]
			) { }

			public Category Filter(IEnumerable<string> packages) {
				if (OptionAllBlueprints) {
					Blueprints = AllBlueprints;
					Hash = -1;
					return this;
				}

				int hash = packages.Aggregate(17, (h, p) => unchecked(h * 23 + p.GetHashCode()));
				if (Blueprints != null && Hash == hash) return this;

				var set = new HashSet<string>(packages);
				Blueprints = new List<Blueprint>(AllBlueprints.Count);
				Hash = hash;

				foreach (var blueprint in AllBlueprints) {
					if (!String.IsNullOrEmpty(blueprint.Package) && !set.Contains(blueprint.Package))
						continue;

					Blueprints.Add(blueprint);
				}
				return this;
			}

		}
	}
}
