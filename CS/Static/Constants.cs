using System;
using System.Linq;

using XRL;
using XRL.Rules;
using XRL.Core;
using XRL.World;
using ConsoleLib.Console;
using Color = UnityEngine.Color;

using static XRL.UI.Options;

namespace Hearthpyre
{
	[HasGameBasedStaticCache]
	public static partial class Static
	{
		// Properties
		public static XRLGame GAME => XRLCore.Core.Game;
		public static GameObject PLAYER => GAME.Player.Body;
		public static Zone ZONE => GAME.ZoneManager.ActiveZone;
		// Random
		[GameBasedStaticCache(CreateInstance = false)]
		static Random random;
		public static Random RANDOM {
			get {
				if (random == null) {
					if (GAME == null) throw new Exception("Game not created yet.");
					random = Stat.GetSeededRandomGenerator("Hearthpyre");
				}
				return random;
			}
		}
		// IDs
		public const string TAG_OBJ = "HearthpyreObject";
		public const string TAG_PNT = "HearthpyrePaintedObject";
		public const string PRT_BLPR = "HearthpyreBlueprint";
		public const string PRT_STLR = "HearthpyreSettler";
		public const string PRT_SCMR = "HearthpyreXyloschemer";
		public const string PRT_CART = "HearthpyreCart";
		public const string PRT_SECT = "HearthpyreSectorWidget";
		public const string EFF_MRG = "HearthpyreMirage";
		public const string POP_TASK = "HearthpyreVillageTask";
		public const string OBJ_SCMR = PRT_SCMR;
		public const string OBJ_BLPR = PRT_BLPR;
		public const string OBJ_SECT = PRT_SECT;
		public const string OBJ_CART = "HearthpyreHandcart";
		public const string FAC_STLR = "HearthpyreSettlers";
		// Options
		public static bool OptionDeployCart => GetOption("OptionHearthpyreDeployCart", "No").EqualsNoCase("Yes");
		public static bool OptionAllBlueprints => GetOption("OptionHearthpyreAllBlueprints", "No").EqualsNoCase("Yes");
		public static bool OptionAllowCombat => GetOption("OptionHearthpyreAllowCombat", "No").EqualsNoCase("Yes");
		public static bool OptionAllDemo => GetOption("OptionHearthpyreAllDemo", "No").EqualsNoCase("Yes");
		public static bool OptionFastStart => GetOption("OptionHearthpyreFastStart", "No").EqualsNoCase("Yes");
		// Sounds
		public const string SND_ADD = "Spark1";
		public const string SND_SEL = "click3";
		public const string SND_DSEL = "click";
		public const string SND_REM = "Clink2";
		public const string SND_ERR = "Bow";
		public const string SND_MENU = "sub_bass_mouseover1";
		public const string SND_TEXT = "SFX_Books_Page Turn_1";
		public const string SND_CLM = "completion";
		public const string SND_ANX = "clairvoyance";
		public const string SND_LVLO = "Level_Up_Other";
		// Colours
		public static Color CLD_BLK { get; private set; }
		public static Color CLD_BLUE { get; private set; }
		public static Color CLD_GRN { get; private set; }
		public static Color CLD_CYAN { get; private set; }
		public static Color CLD_RED { get; private set; }
		public static Color CLD_MAG { get; private set; }
		public static Color CLD_YEL { get; private set; }
		public static Color CLD_WHT { get; private set; }
		public static Color CLD_ORNG { get; private set; }
		public static Color CLB_BLK { get; private set; }
		public static Color CLB_BLUE { get; private set; }
		public static Color CLB_GRN { get; private set; }
		public static Color CLB_CYAN { get; private set; }
		public static Color CLB_RED { get; private set; }
		public static Color CLB_MAG { get; private set; }
		public static Color CLB_YEL { get; private set; }
		public static Color CLB_WHT { get; private set; }
		public static Color CLB_ORNG { get; private set; }
		// Tiles
		public const string ICO_ORG = "Terrain/sw_monument3.bmp";
		public const string ICO_CLM = "Terrain/hp_claim.png";
		// Skills
		public const string GOV_BASE = "HearthpyreGoverning";
		public const string GOV_LOC = "HearthpyreGoverning_Locate";
		public const string GOV_GRFT = "HearthpyreGoverning_Xylogrifter";
		public const double GOV_GRFT_MUL = 0.75d;
		public const string GOV_HRLD = "HearthpyreGoverning_Heraldry";
		public const string GOV_PRT = "HearthpyreGoverning_Porter";
		public const double GOV_PRT_MUL = 0.5d;
		public const string GOV_MYR = "HearthpyreGoverning_Mayor";
		public const string GOV_DST1 = "HearthpyreGoverning_GovernI";
		public const string GOV_DST2 = "HearthpyreGoverning_GovernII";
		public const string GOV_DST3 = "HearthpyreGoverning_GovernIII";
		// View
		public const int PEN_X = 4;
		public const int FILL_X = 25;
		public const int HELP_X = 67;

		public static void LoadColorMap() {
			CLD_BLK = ColorUtility.ColorMap['k'];
			CLD_BLUE = ColorUtility.ColorMap['b'];
			CLD_GRN = ColorUtility.ColorMap['g'];
			CLD_CYAN = ColorUtility.ColorMap['c'];
			CLD_RED = ColorUtility.ColorMap['r'];
			CLD_MAG = ColorUtility.ColorMap['m'];
			CLD_YEL = ColorUtility.ColorMap['w'];
			CLD_WHT = ColorUtility.ColorMap['y'];
			CLD_ORNG = ColorUtility.ColorMap['o'];
			CLB_BLK = ColorUtility.ColorMap['K'];
			CLB_BLUE = ColorUtility.ColorMap['B'];
			CLB_GRN = ColorUtility.ColorMap['G'];
			CLB_CYAN = ColorUtility.ColorMap['C'];
			CLB_RED = ColorUtility.ColorMap['R'];
			CLB_MAG = ColorUtility.ColorMap['M'];
			CLB_YEL = ColorUtility.ColorMap['W'];
			CLB_WHT = ColorUtility.ColorMap['Y'];
			CLB_ORNG = ColorUtility.ColorMap['O'];
		}
	}
}
