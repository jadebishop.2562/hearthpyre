﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using ConsoleLib.Console;
using Hearthpyre.UI;
using System.Linq;
using System.Reflection;
using SimpleJSON;

namespace Hearthpyre
{
	public static partial class Static
	{
		/// <summary>
		/// Levenshtein string distance calc.
		/// </summary>
		public static int StrDist(string str, string txt) {
			str = str.ToLower(); txt = txt.ToLower();
			int sl = str.Length, tl = txt.Length;
			var matrix = new int[sl + 1, tl + 1];

			if (sl == 0) return tl;
			if (tl == 0) return sl;

			for (int i = 0; i <= sl; i++) matrix[i, 0] = i;
			for (int i = 0; i <= tl; i++) matrix[0, i] = i;

			for (int i = 1; i <= sl; i++) {
				for (int j = 1; j <= tl; j++) {
					int cost = (txt[j - 1] == str[i - 1]) ? 0 : 1;

					matrix[i, j] = Math.Min(
						Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1] + 1),
						matrix[i - 1, j - 1] + cost);
				}
			}
			return matrix[sl, tl];
		}
		/// <summary>
		/// Jaro winkler string proximity calc. Shamelessly stolen and modified.
		/// Also buggy. Hmm, don't steal kids.
		/// </summary>
		public static double StrProx(string str1, string str2) {
			str1 = str1.ToLower(); str2 = str2.ToLower();
			int l1 = str1.Length, l2 = str2.Length;
			if (l1 == 0) return l2 == 0 ? 1d : 0d;

			int range = Math.Max(0, Math.Max(l1, l2) / 2 - 1);
			bool[] match1 = new bool[l1], match2 = new bool[l2];

			var common = 0d;
			for (int i = 0; i < l1; i++) {
				for (int j = Math.Max(0, i - range), c = Math.Min(i + range + 1, l2); j < c; j++) {
					if (match2[j] || str1[i] != str2[j]) continue;

					match1[i] = true;
					match2[j] = true;
					common++;
					break;
				}
			}
			if (common <= 0d) return 0d;

			int transposed = 0;
			for (int i = 0, k = 0; i < l1; i++) {
				if (!match1[i]) continue;

				while (!match2[k]) k++;
				if (str1[i] != str2[k])
					transposed++;

				k++;
			}
			transposed /= 2;

			var weight = (common / l1 + common / l2 + (common - transposed) / common) / 3.0;
			if (weight <= 0.7) return weight;

			int lMax = Math.Min(4, Math.Min(str1.Length, str2.Length));
			int lPos = 0;
			while (lPos < lMax && str1[lPos] == str2[lPos]) lPos++;
			if (lPos == 0) return weight;

			return weight + 0.1 * lPos * (1.0 - weight);
		}
		/// <summary>
		/// Wrangle character indexes in RenderStrings of blueprints.
		/// </summary>
		public static string RenderStrToChar(string str) {
			if (str != null && str.Length > 1)
				str = ((char)Convert.ToInt32(str)).ToString();
			return str;
		}
		public static void SplitColorString(string color, out char fg, out char bg) {
			int iFG = -1, iBG = -1;
			for (int i = 0; i < color.Length; i++) {
				var chr = color[i];
				if (chr == '&') iFG = i + 1;
				else if (chr == '^') iBG = i + 1;
			}

			if (iFG >= 0 && iFG < color.Length) fg = color[iFG];
			else fg = 'k';

			if (iBG >= 0 && iBG < color.Length) bg = color[iBG];
			else bg = 'k';
		}
		/// <summary>
		/// Convert string to key.
		/// </summary>
		public static Keys StringToKey(string key) {
			try {
				return (Keys)Enum.Parse(typeof(Keys), key, true);
			} catch (Exception) {
				return Keys.None;
			}
		}
		/// <summary>
		/// Strip the color string of any invalid/unnecessary characters.
		/// </summary>
		public static string FilterColor(string color, int flags = 0) {
			var anchors = new[] { '&', '^', '*' };
			var map = ColorUtility.ColorMap;
			var sb = Strings.SB.Clear();
			var flag = 1;
			foreach (var anchor in anchors) {
				var i = color.LastIndexOf(anchor);
				if (i != -1 && !flags.HasFlag(flag) && map.ContainsKey(color[i + 1]))
					sb.Append(anchor).Append(color[i + 1]);
				flag = flag << 1;
			}

			return sb.ToString();
		}
		/// <summary>
		/// Brand object for later demolition and devalue it.
		/// </summary>
		public static void Brand(GameObject obj) {
			var commerce = obj.GetPart("Commerce") as Commerce;
			if (commerce != null) commerce.Value = 0.01d;

			var tinker = obj.GetPart("TinkerItem") as TinkerItem;
			if (tinker != null) tinker.CanDisassemble = false;

			var examiner = obj.GetPart("Examiner") as Examiner;
			if (examiner != null) examiner.Complexity = 0;

			obj.SetIntProperty(Static.TAG_OBJ, 1);
		}
		/// <summary>
		/// Add air to cell if empty and unpainted.
		/// </summary>
		public static void AddChasm(Cell C) {
			if (C.Objects.Count > 0) return;
			if (!String.IsNullOrEmpty(C.PaintTile)) return;
			if (!String.IsNullOrEmpty(C.PaintRenderString)) return;
			C.AddObject("Air");
		}
		/// <summary>
		/// Remove any chasm objects in cell.
		/// </summary>
		public static void RemoveChasm(Cell C) {
			for (int i = C.Objects.Count - 1; i >= 0; i--) {
				var obj = C.Objects[i];
				if (obj.HasPart("ChasmMaterial")) {
					C.RemoveObject(C.Objects[i]);
				}
			}
		}
		/// <summary>
		/// Resolve and instantiate type.
		/// </summary>
		public static Object ResolveInstance(string typeId) {
			var type = ModManager.ResolveType(typeId);
			return Activator.CreateInstance(type);
		}
		/// <summary>
		/// Get world location from zone ID.
		/// </summary>
		public static int[] WorldLocation(string zoneId) {
			var pos = zoneId.Split('.').Skip(1).Select(x => Int32.Parse(x));
			return new[] {
				pos.ElementAt(0) * 3 + pos.ElementAt(2),
				pos.ElementAt(1) * 3 + pos.ElementAt(3),
				pos.ElementAt(4)
			};
		}
		/// <summary>
		/// Yield coordinates in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<int[]> LazyCardinalFlood(int x, int y, int w, int h, Func<int, int, bool> predicate = null) {
			var queue = new Queue<int[]>();
			var map = new int[w, h];
			queue.Enqueue(new[] { x, y });

			while (queue.Count != 0) {
				int x2 = 1, y2 = 0;
				var SC = queue.Dequeue();
				x = SC[0]; y = SC[1];
				yield return SC;

				for (int i = 0; i < 4; i++) {
					int b = x2, x3 = x + x2, y3 = y + y2;
					x2 = -y2;
					y2 = b;

					if (x3 < 0 || x3 >= w || y3 < 0 || y3 >= h)
						continue;

					if (map[x3, y3] > 0) continue;
					else map[x3, y3] = 1;

					if (predicate == null || predicate(x3, y3))
						queue.Enqueue(new[] { x3, y3 });
				}
			}
		}
		/// <summary>
		/// Yield coordinates in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<int[]> LazyCircle(int cx, int cy, int r) {
			if (r == 0) { yield return new[] { cx, cy }; yield break; }

			var rsq = r * r;
			for (int y = -r; y <= r; y++) {
				var ysq = y * y;
				for (int x = -r; x <= r; x++) {
					if (x * x + ysq <= rsq)
						yield return new[] { cx + x, cy + y };
				}
			}
		}

		/// <summary>
		/// Spirals out from coordinates, yielding each one.
		/// </summary>
		public static IEnumerable<int[]> LazySpiral(int x, int y) {
			int x2 = 1, y2 = 0, l = 1;

			for (int i = 1; true; i++) {
				x += x2;
				y += y2;

				if (i >= l) {
					i = 0;

					int b = x2;
					x2 = -y2;
					y2 = b;

					if (y2 == 0) l++;
				}

				yield return new[] { x, y };
			}
		}

		/// <summary>
		/// Log message.
		/// </summary>
		public static void Log(string message, Exception e = null) {
			message = "HEARTHPYRE - " + message;
			if (e != null) XRLCore.LogError(message, e);
			else XRLCore.Log(message);
		}

		/// <summary>
		/// Round double to integer.
		/// </summary>
		public static int RoundToInt(float value) {
			return (int)Math.Round(value);
		}
		/// <summary>
		/// Round double to integer.
		/// </summary>
		public static int RoundToInt(double value) {
			return (int)Math.Round(value);
		}

		public static void HoloZap<T>(IComponent<T> Component, Cell C = null) {
			Component.PlayWorldSound("Electric", 0.25f);
			if (C == null) C = Component.GetAnyBasisCell();

			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 3; j++) {
					C.ParticleText("&B" + (char)(219 + Stat.RandomCosmetic(0, 4)), 2.5f, 4);
				}
				for (int l = 0; l < 3; l++) {
					C.ParticleText("&c" + (char)(219 + Stat.RandomCosmetic(0, 4)), 3.5f, 3);
				}
			}
		}

		public static void PlayUISound(string clip) {
			IPart.PlayUISound(clip);
		}

		public static void MenuSound() {
			PlayUISound(SND_MENU);
		}

		/// <summary>
		/// Shove some space and lines around a makeshift title since we can't set titles on popups at the moment.
		/// </summary>
		public static string MakeshiftPopupTitle(string title) {
			var spacer = " \u00c4 \u00c4 \u00c4 ";
			return spacer + title + spacer;
		}
	}
}
