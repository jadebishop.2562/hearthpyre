using Qud.API;
using XRL.World;
using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Runtime.Serialization.Formatters.Binary;

namespace Hearthpyre.Dialogue
{
	[Serializable]
	public class Dialogue : Conversation
	{
		// Set from PlayerBeginConversation for now, gives access to speaker in parameterless methods like Choice.Test().
		public static GameObject Speaker;

	}
}
