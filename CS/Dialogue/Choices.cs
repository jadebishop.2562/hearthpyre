using XRL.World;
using XRL.Core;
using XRL.World.Parts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using Genkit;
using XRL.World.ZoneBuilders;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue
{
	[Serializable]
	public class DialogueChoice : ConversationChoice
	{
		public string[] Passages;

		public override ConversationNode Goto(GameObject speaker, bool peekOnly = false) {
			if (peekOnly && Passages != null && string.IsNullOrEmpty(Text)) {
				Text = Passages.GetRandomElement();
				Text = VariableReplace(speaker).ToString();
			}
			return base.Goto(speaker, peekOnly);
		}

		/* Extending ConversationUI.VariableReplace here since've not much choice in the matter.

			Perhaps split off some parameterless chunks of 'at big thing into some public Dictionary<string, Action<StringBuilder, GameObject>>
			Prefix (player, subject, object, etc) decides what gameobject the action receives.
			E.g. dictionary["subject.goodbye"] = (sb, obj) => {}  */
		public virtual StringBuilder VariableReplace(GameObject speaker) {
			var text = new StringBuilder(Text);
			if (text.Contains("=goodbye="))
				text.Replace("=goodbye=", GetGoodbyeText(speaker));

			return text;
		}

		private string GetGoodbyeText(GameObject speaker, string alt = "Live and drink.") {
			var id = speaker.GetPart<ConversationScript>()?.ConversationID;
			if (id == null) return alt;

			Conversation comm;
			if (!ConversationLoader.Loader.ConversationsByID.TryGetValue(id, out comm)) return alt;
			if (!comm.StartNodes.Any()) return alt;

			var choice = comm.StartNodes[0].Choices.FirstOrDefault(x => x.GotoID == "End");
			return choice?.Text ?? alt;
		}
	}

	[Serializable]
	public class AttitudeChoice : DialogueChoice
	{
		public string AcceptID;
		public string RejectID;

		public override ConversationNode Goto(GameObject speaker, bool peekOnly = false) {
			if (peekOnly) {
				var req = 50;
				var feeling = speaker.pBrain.GetPersonalFeeling(PLAYER) ?? 0;
				foreach (var x in speaker.pBrain.FactionMembership) {
					var rep = Factions.GetFeelingFactionToObject(x.Key, PLAYER);
					feeling += (int)Math.Round(x.Value / 100f * rep);
				}

				if (PLAYER.HasSkill("Persuasion_Proselytize"))
					req /= 2;

				GotoID = feeling >= req ? AcceptID : RejectID;
			}
			return base.Goto(speaker, peekOnly);
		}

		/*public override StringBuilder VariableReplace(GameObject speaker) {
			var text = base.VariableReplace(speaker);
			if (text.Contains("=cost="))
				text.Replace("=cost=", Cost.ToString());

			if (text.Contains("=currency="))
				text.Replace("=currency=", Currency.ToLower());

			return text;
		}*/

	}

	// TODO: Add choices for upgrading stock with another token
	public class RetainerTokenChoice : DialogueChoice
	{
		public string Retainer;
		public string Stock;
		public int Chance = 100;

		public override bool Test() {
			if (!PLAYER.HasSkill(GOV_MYR)) return false;

			var speaker = Dialogue.Speaker;
			if (speaker == null || speaker.GetIntProperty("Hero") >= 1) return false;

			var settlement = speaker.GetPart<HearthpyreSettler>()?.Settlement;
			if (settlement == null) return false;
			if (settlement.Retainers[Retainer] != null)
				return false;

			return base.Test();
		}

		public override bool Visit(GameObject speaker, GameObject player, out bool removeChoice, out bool terminateConversation) {
			terminateConversation = false;
			removeChoice = false;
			if (Retainer != null) {
				if (Stock != null) {
					speaker.SetStringProperty("GenericInventoryRestockerPopulationTable", Stock);
					speaker.RequirePart<GenericInventoryRestocker>().Chance = Chance;
				}

				var template = "SpecialVillagerHeroTemplate_" + Retainer;
				if (GameObjectFactory.Factory.Blueprints.ContainsKey(template)) {
					Appoint(speaker, template);
					AddProperties(speaker);
				}


				var settlement = speaker.GetPart<HearthpyreSettler>()?.Settlement;
				if (settlement == null) return false;

				settlement.Retainers[Retainer] = speaker.id;
			}

			return base.Visit(speaker, player, out removeChoice, out terminateConversation);
		}

		public void Appoint(GameObject obj, string special) {
			var name = obj.SetTitle(special);
			var C = obj.CurrentCell;
			var Z = C.ParentZone;
			var visibility = Z.GetVisibility(C.X, C.Y);

			// Dodge level up messages with the wrong name applied by hiding object.
			IPart.XDidY(obj, "become", "a " + Retainer.ToLower(), "!", ColorAsGoodFor: obj);
			Z.SetVisibility(C.X, C.Y, false);
			HeroMaker.MakeHero(obj, null, special);
			obj.RemovePart("HasGuards");
			obj.RemovePart("HasThralls");
			obj.DisplayName = name;
			Z.SetVisibility(C.X, C.Y, visibility);
			PlayUISound(SND_LVLO);
		}

		public void AddProperties(GameObject speaker) {
			if (Retainer == "Warden") {
				speaker.pBrain.setFactionMembership("Wardens", 50);
			} else if (Retainer == "Tinker") {
				speaker.SetIntProperty("CanRecharge", 1);
				speaker.SetIntProperty("CanRepair", 1);
				speaker.SetIntProperty("CanIdentify", 3);
				speaker.AddSkill("Tinkering");
				speaker.AddSkill("Tinkering_Repair");
				speaker.AddSkill("Tinkering_Tinker1");
			}
		}

	}

}
