using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Smoke : GoalHandler
	{
		public GameObject Object { get; set; }
		public GameObject Seat { get; set; }
		public int Turns { get; set; }

		public Smoke(GameObject obj = null, GameObject seat = null, int turns = -1) {
			Object = obj;
			Seat = seat;
			Turns = turns;
		}

		public override void Create() {
			Seat?.Reserve(ParentObject.DistanceTo(Seat) + 5L);
		}

		public override bool Finished() {
			return Object?.CurrentCell == null || Turns == 0;
		}

		public override void TakeAction() {
			if (!TryFindSeat()) {
				// Couldn't find a place to sit on, who the hell do ya think I am?
				// Some kinda hookah smoking BARBARIAN?!
				FailToParent();
				return;
			}

			if (ParentObject.CurrentCell != Seat.CurrentCell) {
				if (Seat.CurrentCell.HasCombatObject()) {
					FailToParent();
					return;
				}
				PushChildGoal(new MoveTo(Seat.CurrentCell));
			} else if (!ParentObject.HasEffect("Sitting")) {
				Flight.StopFlying(ParentObject, ParentObject);

				var chair = Seat.GetPart<Chair>();
				ParentObject.ApplyEffect(new Seated(Seat, chair.EffectiveLevel(), chair.DamageAttributes));
				ParentObject.UseEnergy(1000, "Position");
			} else {
				for (int i = 2; i < 5; i++)
					Object.Smoke(150, 180);

				ParentObject.UseEnergy(1000, "Item");
				ParentObject.FireEvent(Event.New("Smoked", "Object", Object));
				PushChildGoal(new Wait(RANDOM.Next(2, 6)));
				Turns--;
			}
		}

		bool TryFindSeat() {
			if (Seat != null) return true;

			Seat = Object.CurrentCell.LazySpiralObjects(3, Rest.ValidSeat).GetRandomElement();
			if (Seat != null) {
				Seat.Reserve(ParentObject.DistanceTo(Seat) + 5L);
				return true;
			}

			return false;
		}
	}
}
