using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Tinker : GoalHandler
	{
		static string[] Sounds = new[] { "Clink1", "Clink2", "Clink3", "Spark1", "Spark2" };
		public GameObject Object { get; set; }
		public int Turns { get; set; }

		public Tinker(GameObject obj = null, int turns = -1) {
			Object = obj;
			Turns = turns;
		}

		public override void Create() {
			Object?.Reserve(ParentObject.DistanceTo(Object) + 5L);
		}

		public override bool Finished() {
			return Object?.CurrentCell == null || Turns == 0;
		}

		public override void TakeAction() {

			if (ParentObject.CurrentCell.DistanceTo(Object) > 1) {
				if (Object.CurrentCell.HasCombatObject()) {
					FailToParent();
					return;
				}
				PushChildGoal(new MoveTo(Object, true));
			} else {
				if (RANDOM.Next(1, 100) <= 10) {
					Object.pPhysics.PlayWorldSound(Sounds.GetRandomElement(), 0.25f);
				}
				Object.DustPuff();
				PushChildGoal(new Wait(RANDOM.Next(2, 6)));
				Turns--;
			}
		}

	}
}
