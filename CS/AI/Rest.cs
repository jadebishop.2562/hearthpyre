using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Rest : IMovementGoal
	{
		public Cell TargetCell { get; set; }
		public GameObject Target { get; set; }
		public int Turns { get; set; }
		public bool Sleep { get; set; }
		public bool Dropped { get; set; }
		public bool Nocturnal { get; set; }

		public Rest(Cell cell = null, GameObject obj = null, int turns = -1, bool sleep = false) {
			if (obj != null) {
				Target = obj;
				TargetCell = obj.CurrentCell;
			} else {
				TargetCell = cell;
			}
			Turns = turns;
			Sleep = sleep;
		}

		public override void Create() {
			Nocturnal = ParentObject.HasTag("Nocturnal");
			Target?.Reserve(ParentObject.DistanceTo(Target) + 5L);

			if (Target == null && Sleep)
				Target = ParentObject.LazyForeachInventory(x => x.HasPart("Bed")).FirstOrDefault();

			if (Target == null)
				Target = ParentObject.LazyForeachInventory(x => x.HasPart("Chair")).FirstOrDefault();
		}

		public override bool Finished() {
			var result = Turns == 0 || TargetCell == null;
			if (Turns <= -1 && Sleep)
				result = Nocturnal != IsDay();

			if (result && Dropped)
				Dropped = !ParentObject.Take(Target, false, 0);

			return result;
		}

		public override void TakeAction() {
			if (ParentObject.CurrentCell != TargetCell) {
				if (TargetCell.HasCombatObject()) {
					FailToParent();
				} else {
					PushChildGoal(new MoveTo(TargetCell));
				}
			} else if (!ParentObject.HasEffect("Prone", "Sitting")) {
				Flight.StopFlying(ParentObject, ParentObject);
				GameObject asleepOn = Target; // TODO: Still throws when a chair?

				if (Target == null) {
					if (Sleep) ParentObject.ApplyEffect(new Prone(true));
					else ParentObject.ApplyEffect(new Seated(-2));
				} else {
					if (Target.InInventory == ParentObject)
						Dropped = ParentObject.Drop(Target);

					if (Target.HasPart("Bed")) {
						asleepOn = Target;
						ParentObject.ApplyEffect(new Prone(Target, true));
						ParentObject.FireEvent(Event.New("SleptIn", "Object", Target));
					} else if (Target.HasPart("Chair")) {
						var part = Target.GetPart<Chair>();
						ParentObject.ApplyEffect(new Seated(Target, part.EffectiveLevel(), part.DamageAttributes));
					}
				}

				if (Sleep && !ParentObject.HasEffect("Asleep")) {
					var sleepDuration = Math.Max(GetSleepDuration(), 50);
					ParentObject.ForceApplyEffect(new Asleep(asleepOn, sleepDuration, true, true, true));
				}
			}
			Turns -= 1;
			ParentObject.ForfeitTurn();
			//Messaging.EmitMessage(ParentObject, false, ParentObject.DisplayNameOnly + ": Wow this " + (Target?.DisplayNameOnly ?? "ground") + " is " + Turns + "/10 soft!");
		}

		public int GetSleepDuration() {
			var tick = (int)(Calendar.TotalTimeTicks % 1200);
			var result = RANDOM.Next(150, 300) - tick;
			if (tick > 300) result += Calendar.turnsPerDay;

			return result;
		}

		// Starts
		public static bool IsDay() {
			var segment = Calendar.CurrentDaySegment;
			return segment >= 1500 && segment < 10000;
		}

		public static bool ValidSeat(GameObject obj) {
			if (obj.IsReserved()) return false;
			if (!obj.HasPart("Chair")) return false;
			if (obj.CurrentCell.HasCombatObject()) return false;
			return true;
		}
	}
}
