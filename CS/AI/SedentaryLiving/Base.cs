using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	// TODO: Split into single tasks instantiated on Create() or first use, reuse.
	// Save on GC n' prevents this garbage where most of the code is outside the sub-goals and their members.
	public partial class SedentaryLiving : IMovementGoal
	{
		public HearthpyreSettler Settler { get; set; }

		public SedentaryLiving() { }

		public override bool Finished() => false;

		public override void Create() {
			Settler = ParentObject.GetPart<HearthpyreSettler>();
			ParentBrain.StartingCell = null;
			ParentBrain.Wanders = true;
		}

		public override void TakeAction() {
			if (Settler.Settlement != null) {
				// TODO: This'd do better as a classic behaviour/priority node tree, but'm not gonna be the one to implement 'at without pay.
				// TODO: Make target area (home, commons, new sector) a single top level decision with a cooldown.
				// TODO: Have NPCs converse (effect, [talking to Object.DisplayName], flash a speech tile? There's the particle system, probably better.),
				// TODO: Action frequency option to reduce NPC franticness.
				// TODO: Maintain villager specific reachability map -- what can *I* reach? (clearing issues again, rebuild on pathing failure?)
				if (GoToSettlement()) return;
				if (Sleep()) return;
				if (Clean()) return;
				if (Work()) return;
				if (DoTask()) return;
				if (Wander()) return;
			}
			Log("FALLBACK: Wandering randomly.");
			PushChildGoal(new WanderRandomly(RANDOM.Next(5, 10)));
		}

		/// <summary>
		/// Pick up items we've dropped from inventory such as chairs or bed rolls that we sat/slept on.
		/// </summary>
		bool Clean() {
			var dropped = ParentObject.GetStringProperty("Dropped");
			if (string.IsNullOrEmpty(dropped)) return false;

			foreach (var str in dropped.Split(';')) {
				var obj = CurrentZone.findObjectById(str);
				if (obj != null && !obj.IsReserved() && !obj.CurrentCell.HasCombatObject(ParentObject)) {
					PushChildGoal(new Obtain(obj));
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Travel back to our settlement if in zone outside of it.
		/// </summary>
		bool GoToSettlement() {
			var sector = Settler.Settlement.GetSector(CurrentCell.ParentZone);
			if (sector == null) {
				PushChildGoal(new Travel(Settler.Settlement.Prime.ZoneID, teleport: true));
				return true;
			}

			return false;
		}

		/// <summary>
		/// Filter to reachable and unoccupied cells.
		/// </summary>
		bool CellPredicate(Cell cell) {
			if (!cell.IsReachable()) return false;

			var combat = cell.GetCombatObject();
			if (combat != null && combat != ParentObject) return false;

			return true;
		}
	}
}
