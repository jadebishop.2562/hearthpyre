using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		/// <summary>
		/// Wander somewhere in the current zone.
		/// </summary>
		bool Wander() {
			if (Settler.Home != null && RANDOM.Next(100) <= 50) {
				if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
					PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
					return true;
				}

				PushChildGoal(new Wander(Settler.Home.Cells.ToList(), RANDOM.Next(3, 8)));
				return true;
			}

			// Travel to another sector within our settlement.
			// TODO: Move to Explore task.
			if (RANDOM.Next(100) <= 5) {
				var sector = Settler.Settlement.Sectors.GetRandomElement().Value;
				PushChildGoal(new Travel(sector.ZoneID));
				return true;
			}

			PushChildGoal(new Wander(Settler.CurrentSector.Commons.ToList(), RANDOM.Next(3, 8)));
			return true;
		}

	}
}
