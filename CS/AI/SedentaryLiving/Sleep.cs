using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		/// <summary>
		/// Evaluate whether and where this creature should sleep.
		/// </summary>
		bool Sleep() {
			if (ParentObject.HasTagOrProperty("NoSleep") || ParentObject.HasPart("Robot")) return false;
			if (ParentObject.HasTag("Nocturnal") != Rest.IsDay()) return false;

			if (SleepInArea(new[] { CurrentCell }))
				return true;

			if (Settler.Home != null) {
				if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
					PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
					return true;
				}

				if (SleepInArea(Settler.Home.Cells))
					return true;
			}

			return SleepInArea(Settler.CurrentSector.Commons);
		}

		bool BedPredicate(GameObject obj) => obj.HasPart("Bed") && !obj.IsReserved();

		/// <summary>
		/// Find appropriate object/cell in area and push sleep goal.
		/// </summary>
		bool SleepInArea(IEnumerable<Cell> cells) {
			var reachable = cells.Where(CellPredicate);
			var target = ParentObject.CurrentCell.GetFirstObjectWithPart("Bed"); ;
			if (target == null) {
				target = reachable.Select(x => x.GetFirstObject(BedPredicate))
					.Where(x => x != null)
					.GetRandomElement(RANDOM);
			}

			if (target == null) {
				target = reachable.Select(x => x.GetFirstObject(ChairPredicate))
					.Where(x => x != null)
					.GetRandomElement(RANDOM);
			}

			if (target != null) {
				PushChildGoal(new Rest(obj: target, sleep: true));
				return true;
			}

			var floor = reachable.Where(x => x.IsEmpty()).GetRandomElement(RANDOM);
			if (floor != null) {
				PushChildGoal(new Rest(cell: floor, sleep: true));
				return true;
			}

			return false;
		}

	}
}
