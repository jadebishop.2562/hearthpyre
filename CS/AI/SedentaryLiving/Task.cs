using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		public const string TASK_NONE = "None";
		public const string TASK_SIT = "Sit";
		public const string TASK_PRAY = "Pray";
		public const string TASK_SMOKE = "Smoke";
		public const string TASK_REPAIR = "Repair";
		public const string TASK_WANDER = "Wander";
		public const string TASK_EXPLORE = "Explore";

		public string CurrentTask { get; set; }

		bool DoTask() {
			var task = CurrentTask;
			CurrentTask = TASK_NONE;

			if (task == TASK_NONE) {
				task = PopulationManager.RollOneFrom(POP_TASK).Blueprint;
			}

			if (task == TASK_SIT && Sit()) return true;
			if (task == TASK_SMOKE && Smoke()) return true;
			if (task == TASK_EXPLORE && Explore()) return true;

			return false;
		}

		bool Explore() {
			//var region = Villager.Village.Sectors.Values.GetRandomElement(random);
			var region = Settler.Settlement.Sectors.GetRandomElement(RANDOM).Value;
			if (region.ZoneID == CurrentZone.ZoneID) return false;

			PushChildGoal(new Travel(region.ZoneID, RANDOM.Next(5, 75), RANDOM.Next(3, 22)));
			return true;
		}
	}
}
