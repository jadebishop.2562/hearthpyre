using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		const int SIT_DUR_MIN = 40;
		const int SIT_DUR_MAX = 80;

		/// <summary>
		/// Evaluate whether and where this creature should sit.
		/// </summary>
		bool Sit() {
			if (Settler.Home != null && RANDOM.Next(100) <= 75) {
				if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
					PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
					return true;
				}

				if (SitInArea(Settler.Home.Cells))
					return true;
			}

			return SitInArea(Settler.CurrentSector.Commons);
		}

		bool ChairPredicate(GameObject obj) => obj.HasPart("Chair") && !obj.IsReserved();

		/// <summary>
		/// Find appropriate object/cell in area and push sit goal.
		/// </summary>
		bool SitInArea(IEnumerable<Cell> cells) {
			var reachable = cells.Where(CellPredicate);
			var target = ParentObject.CurrentCell.GetFirstObjectWithPart("Chair");
			if (target == null) {
				target = reachable.Select(x => x.GetFirstObject(ChairPredicate))
					.Where(x => x != null)
					.GetRandomElement(RANDOM);
			}

			if (target != null) {
				PushChildGoal(new Rest(obj: target, turns: RANDOM.Next(SIT_DUR_MIN, SIT_DUR_MAX)));
				return true;
			}

			var floor = reachable.Where(x => x.IsEmpty()).GetRandomElement(RANDOM);
			if (floor != null) {
				PushChildGoal(new Rest(cell: floor, turns: RANDOM.Next(SIT_DUR_MIN, SIT_DUR_MAX)));
				return true;
			}

			return false;
		}

	}
}
