using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		bool Smoke() {
			if (ParentObject.HasPart("Robot")) return false;

			if (Settler.Home != null && RANDOM.Next(100) <= 75) {
				if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
					PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
					return true;
				}

				if (SmokeInArea(Settler.Home.Cells))
					return true;
			}

			return SmokeInArea(Settler.CurrentSector.Commons);
		}

		bool SmokeInArea(IEnumerable<Cell> cells) {
			GameObject prime = null;
			GameObject auxil = null;
			var reachable = cells.Where(CellPredicate);

			foreach (var cell in reachable) {
				var obj = cell.GetFirstObjectWithPart("Hookah");
				if (obj == null) continue;

				var seat = cell.LazySpiralObjects(3, Rest.ValidSeat).GetRandomElement();
				if (seat != null) {
					prime = obj;
					auxil = seat;
					break;
				}
			}

			if (prime != null) {
				PushChildGoal(new Smoke(prime, auxil, RANDOM.Next(10, 30)));
				return true;
			}

			return false;
		}
	}
}
