using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Core;
using XRL.Rules;
using XRL.World;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public partial class SedentaryLiving : IMovementGoal
	{
		/// <summary>
		/// Retainers work: tend shops, tinker and patrol.
		/// </summary>
		// TODO: Account for nocturnals.
		// TODO: Assignments for regular settlers.
		bool Work() {
			if (Calendar.CurrentDaySegment < 4000 || Calendar.CurrentDaySegment >= 8000) return false;
			if (Calendar.TotalTimeTicks % 8400 >= 6000) return false;

			var retainers = Settler.Settlement.Retainers;
			if (ParentObject.id == retainers["Merchant"]) return TendShop();
			if (ParentObject.id == retainers["Tinker"]) return Tinker();
			if (ParentObject.id == retainers["Warden"]) return Patrol();

			return false;
		}

		bool TendShop() {
			if (Settler.Home == null) return false;

			if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
				PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
				return true;
			}

			return SitInArea(Settler.Home.Cells);
		}

		bool Tinker() {
			if (Settler.Home == null) return false;

			if (Settler.Home.Sector.ZoneID != CurrentZone.ZoneID) {
				PushChildGoal(new Travel(Settler.Home.Sector.ZoneID));
				return true;
			}

			if (RANDOM.Next(1, 100) >= 50 && TinkerInArea(Settler.Home.Cells))
				return true;

			return SitInArea(Settler.Home.Cells);
		}

		bool TinkerInArea(IEnumerable<Cell> cells) {
			var items = new List<GameObject>();
			var reachable = cells.Where(CellPredicate);

			foreach (var cell in reachable) {
				if (cell.HasCombatObject()) continue;

				var item = cell.GetFirstObject(x => x.HasPart("TinkerItem") && !x.IsReserved());
				if (item != null) items.Add(item);
			}

			if (items.Count >= 1) {
				PushChildGoal(new Tinker(obj: items.GetRandomElement(RANDOM), turns: RANDOM.Next(20, 40)));
				return true;
			}

			return false;
		}

		// TODO: Patrol next adjacent sector instead of randomly.
		bool Patrol() {
			//var sector = Villager.Village.Sectors.Values.GetRandomElement();
			var sector = Settler.Settlement.Sectors.GetRandomElement().Value;
			PushChildGoal(new Wait(RANDOM.Next(20, 60)));
			int x = 40 + RANDOM.Next(-5, 5), y = 12 + RANDOM.Next(-3, 3);
			PushChildGoal(new Travel(sector.ZoneID, x, y));
			return true;
		}
	}
}
