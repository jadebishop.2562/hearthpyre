using System;
using System.Linq;
using System.Collections.Generic;
using Hearthpyre.Realm;
using XRL;
using XRL.Rules;
using XRL.World;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using XRL.World.Parts;
using XRL.World.Effects;
using XRL.World.Capabilities;
using Seated = XRL.World.Effects.Sitting;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	public class Wander : IMovementGoal
	{
		public List<Cell> Area { get; set; }
		public int Turns { get; set; }

		public Wander(List<Cell> area, int turns = -1) {
			Area = area;
			Turns = turns;
		}

		public override bool Finished() {
			return Turns == 0;
		}

		public override void TakeAction() {
			var cell = Area.GetRandomElement();
			if (cell == null) {
				FailToParent();
				return;
			}

			for (int i = 0; i < 100 && !CellPredicate(cell); i++) {
				cell = Area.GetRandomElement();
			}

			PushChildGoal(new Wait(RANDOM.Next(10, 30)));
			PushChildGoal(new MoveTo(cell));
			Turns--;
		}

		public bool CellPredicate(Cell cell) {
			if (!cell.IsReachable()) return false;
			if (!cell.IsEmptyFor(ParentObject)) return false;
			//if (cell.IsOccludingFor(ParentObject)) return false;
			//if (cell.NavigationWeight(ParentObject, true) > 0) return false;
			//if (cell.IsSolidFor(ParentObject)) return false;

			return true;
		}
	}
}
