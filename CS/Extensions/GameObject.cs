using System;
using System.Collections.Generic;
using System.Linq;

using XRL;
using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.ZoneBuilders;
using XRL.World.Parts;
using HistoryKit;
using ConsoleLib.Console;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	[HasGameBasedStaticCache]
	public static partial class Extensions
	{
		// Store object reservations to dodge AI collisions.
		// Cleared in governing skill load, imperfect solution but's no static global load listener in the game.
		// Static cache fixes this, thank you gnarf.
		[GameBasedStaticCache]
		public static Dictionary<string, long> Reservations;
		/// <summary>
		/// Check if an object has been reserved.
		/// </summary>
		public static bool IsReserved(this GameObject obj) {
			// TODO: IsReservedFor()
			long reserved;
			if (!Reservations.TryGetValue(obj.id, out reserved))
				return false;

			var turns = GAME.Turns;
			return reserved - turns > 0;
		}
		/// <summary>
		/// Reserve an object for a set amount of turns.
		/// AI will skip over this object for the duration, giving the reserver temporary exclusive use of the object.
		/// </summary>
		public static void Reserve(this GameObject obj, long turns = 5L) {
			//obj.SetLongProperty("Reserved", GAME.Turns + turns);
			Reservations[obj.id] = GAME.Turns + turns;
		}
		/// <summary>
		/// Pick up an object and remove dropped property.
		/// </summary>
		public static bool Take(this GameObject obj, GameObject item, bool silent = false, int energy = -1) {
			var e = Event.New("CommandTakeObject", "Object", item);
			if (silent) e.SetSilent(true);
			if (energy != -1) e.SetParameter("EnergyCost", energy);

			var result = obj.FireEvent(e);
			if (result) {
				var dropped = obj.GetStringProperty("Dropped");
				if (!string.IsNullOrEmpty(dropped)) {
					if (item.id == dropped) obj.RemoveStringProperty("Dropped");
					else obj.SetStringProperty("Dropped", dropped.Replace(";" + item.id, ""));
				}
			}

			return result;
		}
		/// <summary>
		/// Drop an object and set dropped property for picking it up again later.
		/// </summary>
		public static bool Drop(this GameObject obj, GameObject item, bool silent = false, bool forced = false) {
			var e = Event.New("CommandDropObject", "Object", item);
			if (silent) e.SetSilent(true);
			if (forced) e.SetParameter("Forced", 1);

			var result = obj.FireEvent(e);
			if (result) {
				var dropped = obj.GetStringProperty("Dropped");
				if (string.IsNullOrEmpty(dropped)) dropped = item.id;
				else dropped += ";" + item.id;

				obj.SetStringProperty("Dropped", dropped);
			}

			return result;
		}
		/// <summary>
		/// Loop through target's Inventory/Equipped items, yielding items matching predicate.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachItems(this GameObject target, Func<GameObject, bool> predicate) {
			foreach (var equipped in LazyForeachInventory(target, predicate)) {
				yield return equipped;
			}

			var body = target.GetPart("Body") as Body;
			if (body == null || body._Body == null) yield break;

			foreach (var equipped in LazyForeachEquip(body._Body, predicate)) {
				yield return equipped;
			}
		}
		/// <summary>
		/// Loop through target's Inventory items, yielding items matching predicate.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachInventory(this GameObject target, Func<GameObject, bool> predicate) {
			var inventory = target.GetPart("Inventory") as Inventory;
			if (inventory == null || inventory.Objects == null) yield break;


			foreach (var obj in inventory.Objects) {
				if (predicate(obj)) yield return obj;
			}
		}
		/// <summary>
		/// Recursively loop through all equipped items on object.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachEquip(this GameObject obj, Func<GameObject, bool> predicate) {
			return LazyForeachEquip(obj.GetPart<Body>().GetBody(), predicate);
		}
		/// <summary>
		/// Recursively loop through all equipped items on and under provided limb.
		/// </summary>
		public static IEnumerable<GameObject> LazyForeachEquip(this BodyPart limb, Func<GameObject, bool> predicate) {
			if (limb == null) yield break;
			if (limb.Equipped != null && limb.IsFirstSlotForEquipped()) {
				if (predicate(limb.Equipped)) yield return limb.Equipped;
			}

			if (limb.Parts == null) yield break;
			foreach (var part in limb.Parts) {
				foreach (var equipped in LazyForeachEquip(part, predicate))
					yield return equipped;
			}
		}
		/// <summary>
		/// Display formatted message if target is player.
		/// </summary>
		public static bool PlayerMessage(this GameObject target, string format, params object[] args) {
			if (!target.IsPlayer()) return false;
			GAME.Player.Messages.Add(string.Format(format, args));
			return true;
		}
		/// <summary>
		/// Display formatted popup if target is player.
		/// </summary>
		public static bool PlayerPopup(this GameObject target, string str) {
			if (!target.IsPlayer()) return false;
			Popup.Show(str);
			return true;
		}
		/// <summary>
		/// Re-equip all items with part to reflect potential stat changes.
		/// </summary>
		public static void UpdateEquipped(this GameObject obj, string part, bool AsMinEvent = true) {
			GameManager.Instance.gameQueue.queueTask(() => {
				obj.GetPart<Body>()?.ForeachPart(x => {
					var eqp = x.Equipped;
					if (eqp == null || !eqp.HasPart(part))
						return;

					UnequippedEvent.Send(eqp, obj);
					EquippedEvent.Send(obj, eqp, null);
					/*if (AsMinEvent) {
						eqp.HandleEvent(UnequippedEvent.FromPool(obj, eqp));
						eqp.HandleEvent(EquippedEvent.FromPool(obj, eqp));
					} else {
						eqp.FireEvent(Event.New("Unequipped", "UnequippingObject", obj));
						eqp.FireEvent(Event.New("Equipped", "EquippingObject", obj, "BodyPart", x));
					}*/
				});
			});
		}
		/// <summary>
		/// Get total weight of equipment and inventory.
		/// </summary>
		public static int GetTotalWeight(this GameObject obj) {
			var result = 0;

			var inventory = obj.GetPart<Inventory>();
			if (inventory != null) result += inventory.GetWeight();

			var body = obj.GetPart<Body>();
			if (body != null) result += body.GetWeight();

			return result;
		}
		/// <summary>
		/// Set title from hero templates/specializations.
		/// </summary>
		public static string SetTitle(this GameObject obj, string special) {
			var color = HeroMaker.ResolveTemplateTag(obj, "HeroNameColor", "&M", new string[0], new[] { special });
			var name = obj.pRender.DisplayName.Split(',')[0];

			// Wow let's bust open the name maker with some more mod hooks... some other time
			if (special.EndsWith("Merchant")) name += ", village merchant";
			else if (special.EndsWith("Tinker")) name += ", village tinker";
			else if (special.EndsWith("Warden")) name = "Warden " + name;

			return color + ColorUtility.StripFormatting(name);
		}
		/// <summary>
		/// Get color from tile|string.
		/// </summary>
		[Obsolete]
		public static string GetColor(this Render render) {
			if (!String.IsNullOrEmpty(render.TileColor))
				return render.TileColor;
			return render.ColorString;
		}
		/// <summary>
		/// Leave copy of tile as particle for duration.
		/// </summary>
		[Obsolete]
		public static void TileParticleBlip(this GameObject obj, int duration, bool ignoreVis = false) {
			var C = obj.CurrentCell;
			if (!ignoreVis && !C.IsVisible()) return;

			var render = obj.pRender;
			XRLCore.ParticleManager.AddTile(render.Tile, render.GetColor(), render.DetailColor, C.X, C.Y, 0f, 0f, duration, 0f, 0f);
		}
		/// <summary>
		/// Get possessor of item, whether equipped or inventory.
		/// </summary>
		public static GameObject GetBearer(this GameObject obj) {
			var physics = obj.pPhysics;
			return physics.InInventory ?? physics.Equipped;
		}
		/// <summary>
		/// Get ability by command, adds a null-check for the dictionary.
		/// </summary>
		public static ActivatedAbilityEntry GetAbility(this ActivatedAbilities abilities, string command) {
			if (abilities.AbilityByGuid == null) return null;

			foreach (var pair in abilities.AbilityByGuid) {
				if (pair.Value.Command != command) continue;
				return pair.Value;
			}

			return null;
		}

	}
}
