using System;
using System.Collections.Generic;

using XRL.Core;
using XRL.World;
using XRL.Rules;
using ConsoleLib.Console;
using static Hearthpyre.Static;
using Color = UnityEngine.Color;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Make console character imitate appearance of blueprint design.
		/// </summary>
		public static void Imitate(this ConsoleChar chr, GameObjectBlueprint design) {
			var render = design.GetPart("Render");
			if (render == null) return;

			string tile, color, atlas;
			if (design.Tags.TryGetValue("PaintedFence", out tile)) {
				var sb = Strings.SB.Clear();
				if (design.Tags.TryGetValue("PaintedFenceAtlas", out atlas))
					sb.Append(atlas);
				else sb.Append("Tiles/");
				sb.Append(tile).Append("_ew");
				if (design.Tags.TryGetValue("PaintedFenceExtension", out tile))
					sb.Append(tile);
				else sb.Append(".bmp");
				chr.Tile = sb.ToString();
			} else if (design.Tags.TryGetValue("PaintedWall", out tile)) {
				var sb = Strings.SB.Clear();
				if (design.Tags.TryGetValue("PaintedWallAtlas", out atlas))
					sb.Append(atlas);
				else sb.Append("Tiles/");
				sb.Append(tile).Append("-00000000");
				if (design.Tags.TryGetValue("PaintedWallExtension", out tile))
					sb.Append(tile);
				else sb.Append(".bmp");
				chr.Tile = sb.ToString();
			} else if (render.Parameters.TryGetValue("Tile", out tile)) {
				chr.Tile = tile;
			} else if (render.Parameters.TryGetValue("RenderString", out tile)) {
				chr.Char = Static.RenderStrToChar(tile)[0];
			}

			if (Globals.RenderMode == RenderModeType.Tiles && render.Parameters.TryGetValue("TileColor", out color)) {
				chr.SetColorString(color);
			} else if (render.Parameters.TryGetValue("ColorString", out color)) {
				chr.SetColorString(color);
			}
			if (render.Parameters.TryGetValue("DetailColor", out color))
				chr.Detail = ColorUtility.ColorMap[color[0]];
		}
		/// <summary>
		/// Set tile and colors of console character.
		/// </summary>
		public static void Display(this ConsoleChar chr, string tile, Color fg, Color dt) {
			chr.Tile = tile;
			chr.Foreground = fg;
			chr.Background = CLD_BLK;
			chr.Detail = dt;
		}
		public static void SetColorString(this ConsoleChar chr, string color) {
			var i = color.LastIndexOf('&');
			if (i != -1) chr.Foreground = ColorUtility.ColorMap[color[i + 1]];
			else chr.Foreground = CLD_BLK;

			i = color.LastIndexOf('^');
			if (i != -1) chr.Background = ColorUtility.ColorMap[color[i + 1]];
			else chr.Background = CLD_BLK;
		}
		/// <summary>
		/// Write aligned string to buffer.
		/// </summary>
		/// <returns>Icon position or start of string if none.</returns>
		[Obsolete]
		public static int Write(this ScreenBuffer sb, float align, string str) {
			int x = sb.X, result = 0, icon;

			var len = str.Quantify(out icon);
			if (align == 1f) {
				sb.X = sb.Width - len - x;
			} else if (align != 0f) {
				sb.X = RoundToInt(sb.Width * align - len * 0.5f);
			}

			result = sb.X + icon;
			sb.Write(str);
			sb.X = x;

			return result;
		}
		/// <summary>
		/// Write aligned string to buffer.
		/// </summary>
		/// <returns>Icon position or start of string if none.</returns>
		[Obsolete]
		public static int Write(this ScreenBuffer sb, int x1, int x2, float align, string str) {
			int old = sb.X, w = x2 - x1 + 1, result = 0, icon;

			var len = str.Quantify(out icon);
			if (align == 1f) {
				sb.X = x2 - len;
			} else if (align != 0f) {
				sb.X = x1 + RoundToInt(w * align - len * 0.5);
			} else {
				sb.X = x1;
			}

			result = sb.X + icon;
			sb.Write(str);
			sb.X = old;

			return result;
		}
		/// <summary>
		/// Write string to position without moving cursor.
		/// </summary>
		public static void Write(this ScreenBuffer sb, int x, int y, string str) {
			int ox = sb.X, oy = sb.Y;
			sb.Goto(x, y);
			sb.Write(str);
			sb.Goto(ox, oy);
		}
		/// <summary>
		/// Set buffer characters directly with optional colour.
		/// </summary>
		public static void BasicWrite(this ScreenBuffer sb, int x, int y, string str, Color foreground) {
			for (int i = 0; i < str.Length; i++) {
				var chr = sb[x + i, y];
				chr.Char = str[i];
				chr.Foreground = foreground;
				chr.Background = CLD_BLK;
			}
		}
		/// <summary>
		/// Write strings with equidistant spacing.
		/// </summary>
		/// <returns>Length along the X axis.</returns>
		/*public static int[] Write(this ScreenBuffer sb, int width, params string[] arr) {
			//var lengths = arr.Select(x => x.QudLen());
			var result = new int[arr.Length];
			var space = width - arr.Aggregate(0, (l, str) => l + str.QudLen());
			var rest = space % (arr.Length - 1);
			space /= arr.Length - 1;

			for (int i = 0, c = arr.Length; i < c; i++) {
				if (i != 0) sb.X += space;
				if (i == c - 1) sb.X += rest;
				result[i] = sb.X;
				sb.Write(arr[i]);
			}
			return result;
		}
		/// <summary>
		/// Write array of strings with centered spacing.
		/// </summary>
		/// <returns>Length along the X axis.</returns>
		public static int[] WriteAligned(this ScreenBuffer sb, int width, params string[] arr) {
			//var lengths = arr.Select(x => x.QudLen());
			var result = new int[arr.Length];
			var align = 1.0 / (arr.Length - 1);
			var offset = sb.X;

			for (int i = 0, c = arr.Length; i < c; i++) {
				var str = arr[i];
				int ico;
				var len = str.Quantify(out ico);
				if (i == c - 1) {
					sb.X = width + offset - len;
				} else if (i != 0) {
					sb.X = (width * (align * i) - len * 0.5).RoundToInt();
				}
				result[i] = sb.X + ico;
				sb.Write(arr[i]);
			}
			return result;
		}*/
		/// <summary>
		/// Yield characters in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<ConsoleChar> LazyCardinalFlood(this ScreenBuffer sb, int x, int y, Func<ConsoleChar, bool> predicate = null) {
			IEnumerable<int[]> flood;
			if (predicate == null)
				flood = Static.LazyCardinalFlood(x, y, sb.Width, sb.Height);
			else
				flood = Static.LazyCardinalFlood(x, y, sb.Width, sb.Height, (x2, y2) => predicate(sb[x2, y2]));

			foreach (var pair in flood) {
				yield return sb[pair[0], pair[1]];
			}
		}
		/// <summary>
		/// Yield characters in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<ConsoleChar> LazyCircle(this ScreenBuffer sb, int x, int y, int d) {
			var flood = Static.LazyCircle(x, y, d);
			foreach (var pair in flood) {
				yield return sb[pair[0], pair[1]];
			}
		}
		/// <summary>
		/// Draw line in buffer.
		/// </summary>
		public static void HeaderLine(this ScreenBuffer sb, Color foreground) {
			for (int i = 0, c = sb.Width; i < c; i++) {
				var chr = sb[i, sb.Y];
				chr.Foreground = foreground;
				chr.Background = CLD_BLK;
				chr.Char = '\u00C4';

				//if (i == 0) chr.Char = '\u00C0';
				//else if (i == c - 1) chr.Char = '\u00D9';
				//else 
			}
		}
		public static void FoldDown(this ScreenBuffer sb, int x1, int y1, int x2, int y2) {
			for (int i = x1; i <= x2; i++) {
				var b = sb[i, y2];

				b.Char = '\u00C4';
				b.Foreground = CLD_WHT;
				b.Background = CLD_BLK;
			}
			for (int i = y1; i <= y2; i++) {
				var l = sb[x1, i];
				var r = sb[x2, i];

				l.Char = '\u00B3';
				l.Foreground = CLD_WHT;
				l.Background = CLD_BLK;
				r.Char = '\u00B3';
				r.Foreground = CLD_WHT;
				r.Background = CLD_BLK;
			}
			sb[x1, y1].Char = '\u00C2';
			sb[x2, y1].Char = '\u00C2';
			sb[x1, y2].Char = '\u00C0';
			sb[x2, y2].Char = '\u00D9';
		}
		public static void FoldUp(this ScreenBuffer sb, int x1, int y1, int x2, int y2) {
			for (int i = x1; i <= x2; i++) {
				var t = sb[i, y1];

				t.Char = '\u00C4';
				t.Foreground = CLD_WHT;
				t.Background = CLD_BLK;
			}
			for (int i = y1; i <= y2; i++) {
				var l = sb[x1, i];
				var r = sb[x2, i];

				l.Char = '\u00B3';
				l.Foreground = CLD_WHT;
				l.Background = CLD_BLK;
				r.Char = '\u00B3';
				r.Foreground = CLD_WHT;
				r.Background = CLD_BLK;
			}
			sb[x1, y1].Char = '\u00DA';
			sb[x2, y1].Char = '\u00BF';
			sb[x1, y2].Char = '\u00C1';
			sb[x2, y2].Char = '\u00C1';
		}
	}
}
