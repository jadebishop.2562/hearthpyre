using System;
using System.Text;
using System.Collections.Generic;
using XRL.Core;
using XRL;
using XRL.Rules;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Truncate string if exceeding length with optional suffix at the cutoff (like an ellipsis).
		/// Does not throw index-based exceptions. Simple string wrangling causing range exceptions is a hyper-correct abomination, you heard me.
		/// </summary>
		/// <param name="str">String.</param>
		/// <param name="len">Length.</param>
		/// <param name="suf">Suffix.</param>
		public static string Truncate(this string str, int len, string suf = "") {
			if (str.Length <= len) return str;
			var sb = Strings.SB.Clear();

			len -= suf.Length;
			sb.Append(str.Substring(0, len));
			return sb.Append(suf).ToString();
		}
		/// <summary>
		/// Count the length of the string, excluding any special colouring characters.
		/// </summary>
		public static int Quantify(this string str) {
			int ico;
			return Quantify(str, out ico);
		}
		/// <summary>
		/// Count the length of the string, excluding any special colouring characters.
		/// Output the index of last NUL character.
		/// </summary>
		public static int Quantify(this string str, out int ico) {
			var result = 0;
			var parsing = false;
			char c = '\0';
			ico = 0;
			for (int i = 0; i < str.Length; i++) {
				if (parsing) {
					if (c == str[i])
						result++; // Double && or ^^ is literal & or ^.
					parsing = false;
					continue;
				}
				c = str[i];
				if (c == '&' || c == '^') {
					parsing = true;
				} else {
					if (c == '\0') ico = i;
					result++;
				}
			}
			return result;
		}
		/// <summary>
		/// Cause a side effect on each element in the enumeration.
		/// </summary>
		/*public static void For<T>(this IEnumerable<T> enumerable, Action<int, T> action) {
			int i = 0;
			foreach (T item in enumerable) action(i++, item);
		}
		/// <summary>
		/// Cause a side effect on each element in the enumeration.
		/// </summary>
		public static void For<T>(this IEnumerable<T> enumerable, Action<T> action) {
			foreach (T item in enumerable) action(item);
		}*/
		/// <summary>
		/// Insert spaces in Pascal or Camel case string.
		/// Optionally lower the case of affected capitals.
		/// </summary>
		public static string Spaceify(this string str, bool lower = false) {
			var sb = Strings.SB.Clear();
			char last = '\0';
			foreach (var c in str) {
				if (Char.IsUpper(c) && Char.IsLower(last))
					sb.Append(' ').Append(lower ? Char.ToLower(c) : c);
				else
					sb.Append(c);
				last = c;
			}
			return sb.ToString();
		}
		/// <summary>
		/// Check for binary flag.
		/// </summary>
		public static bool HasFlag(this int status, int flag) {
			return (status & flag) == flag;
		}
		/// <summary>
		/// Check for any binary flag.
		/// </summary>
		public static bool HasAnyFlag(this int status, int flags) {
			return (status & flags) > 0;
		}
		/// <summary>
		/// Grow box separately per dimension.
		/// </summary>
		public static Box Grow(this Box box, int x, int y) {
			return new Box(box.x1 - x, box.y1 - y, box.x2 + x, box.y2 + y);
		}
		/// <summary>
		/// Translate box location.
		/// </summary>
		public static Box Translate(this Box box, int x, int y) {
			return new Box(box.x1 + x, box.y1 + y, box.x2 + x, box.y2 + y);
		}
		/// <summary>
		/// Set new location while preserving dimensions.
		/// </summary>
		public static Box Retranslate(this Box box, int x, int y) {
			return new Box(x, y, x + box.x2 - box.x1, y + box.y2 - box.y1);
		}
	}
}
