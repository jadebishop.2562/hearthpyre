using System;
using System.Linq;
using System.Collections.Generic;
using XRL.World;
using Genkit;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	public static partial class Extensions
	{
		/// <summary>
		/// Construct an object, "filling" any hole-like objects under it.
		/// </summary>
		public static GameObject Construct(this Cell cell, string blueprint) {
			if (!cell.IsClear()) return null;

			var obj = GameObjectFactory.Factory.CreateObject(blueprint);
			if (obj.Blueprint != blueprint) {
				obj.Destroy();
				return null;
			}

			return Construct(cell, obj);
		}
		/// <summary>
		/// Construct an object, "filling" any hole-like objects under it.
		/// </summary>
		public static GameObject Construct(this Cell cell, GameObject obj) {
			Brand(obj);
			RemoveChasm(cell);
			return cell.AddObject(obj);
		}
		/// <summary>
		/// Demolish an object, replacing it with air if the cell was empty and unpainted.
		/// </summary>
		public static bool Demolish(this Cell cell, GameObject obj) {
			if (obj == null) return false;

			cell.RemoveObject(obj);
			AddChasm(cell);
			return obj.Destroy();
		}
		/// <summary>
		/// Demolish objects that match the predicate, replacing it with air if the cell was empty and unpainted.
		/// </summary>
		public static bool Demolish(this Cell cell, Func<GameObject, bool> predicate) {
			var objects = cell.Objects;
			var result = false;

			for (int i = objects.Count - 1; i >= 0; i--) {
				var obj = objects[i];
				if (predicate(obj)) {
					result |= cell.Demolish(obj);
				}
			}

			return result;
		}
		/// <summary>
		/// Get cell directly above provided.
		/// </summary>
		public static Cell Above(this Cell cell) {
			return cell.ParentZone.GetZoneFromDirection("U")?.GetCell(cell.X, cell.Y);
		}
		/// <summary>
		/// Get cell directly below provided.
		/// </summary>
		public static Cell Below(this Cell cell) {
			return cell.ParentZone.GetZoneFromDirection("D")?.GetCell(cell.X, cell.Y);
		}
		/// <summary>
		/// Check if cell is open for placement. Ignores dead air.
		/// </summary>
		public static bool IsClear(this Cell cell) {
			if (cell.Objects.Count == 0)
				return true;

			for (int i = 0; i < cell.Objects.Count; i++) {
				var obj = cell.Objects[i];
				if (obj.pRender != null) {
					if (obj.HasPart("ChasmMaterial")) continue;
					if (obj.HasPart("StairsDown")) return false;
					if (obj.HasPart("StairsUp")) return false;
					if (obj.HasPart("Combat")) return false;
					if (obj.HasTag("Door")) return false;
					if (obj.pPhysics.Solid) return false;
				}
			}
			return true;
		}
		/// <summary>
		/// Check if cell has wall or door.
		/// </summary>
		public static bool IsStructure(this Cell cell) {
			if (cell.Objects.Count == 0)
				return false;

			for (int i = 0; i < cell.Objects.Count; i++) {
				var obj = cell.Objects[i];
				if (obj.pRender != null) {
					if (obj.HasTag("Wall")) return true;
					if (obj.HasTag("Door")) return true;
				}
			}
			return false;
		}
		/// <summary>
		/// Spirals out from cell, yielding each one.
		/// </summary>
		public static IEnumerable<Cell> LazySpiral(this Cell cell, int radius) {
			var Z = cell.ParentZone;
			int x = cell.X, y = cell.Y;
			int x2 = 1, y2 = 0, l = 1;

			for (int i = 0, p = 1, c = radius * radius - 1; i < c; i++, p++) {
				x += x2;
				y += y2;

				if (p >= l) {
					p = 0;

					int b = x2;
					x2 = -y2;
					y2 = b;

					if (y2 == 0) l++;
				}

				var result = Z.GetCell(x, y);
				if (result != null)
					yield return result;
			}
		}
		/// <summary>
		/// Spirals out from cell, yielding each object.
		/// </summary>
		public static IEnumerable<GameObject> LazySpiralObjects(this Cell cell, int radius, Func<GameObject, bool> predicate) {
			foreach (var element in LazySpiral(cell, radius)) {
				foreach (var obj in element.Objects) {
					if (predicate(obj))
						yield return obj;
				}
			}
		}
		/// <summary>
		/// Yield objects in cardinal adjacent cells.
		/// </summary>
		public static IEnumerable<GameObject> LazyCardinalObjects(this Cell cell, string part = null) {
			var Z = cell.ParentZone;
			int x = cell.X, y = cell.Y;
			int x2 = 1, y2 = 0;

			for (int i = 0; i < 4; i++) {
				int b = x2;
				x2 = -y2;
				y2 = b;

				var TC = Z.GetCell(x + x2, y + y2);
				if (TC == null) continue;

				for (int k = 0; k < TC.Objects.Count; k++) {
					if (part == null || TC.Objects[k].HasPart(part))
						yield return TC.Objects[k];
				}
			}
		}
		/// <summary>
		/// Yield cells in a circular pattern from origin.
		/// </summary>
		public static IEnumerable<Cell> LazyCircle(this Cell cell, int r) {
			var Z = cell.ParentZone;
			int w = Z.Width, h = Z.Height;
			var flood = Static.LazyCircle(cell.X, cell.Y, r);
			foreach (var pair in flood) {
				int x = pair[0], y = pair[1];
				if (x >= 0 && y >= 0 && x < w && y < h)
					yield return Z.GetCell(x, y);
			}
		}
		/// <summary>
		/// Yield cells in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<Cell> LazyCardinalFlood(this Cell cell, Func<Cell, bool> predicate = null) {
			var Z = cell.ParentZone;
			IEnumerable<int[]> flood;
			if (predicate == null)
				flood = Static.LazyCardinalFlood(cell.X, cell.Y, Z.Width, Z.Height);
			else
				flood = Static.LazyCardinalFlood(cell.X, cell.Y, Z.Width, Z.Height, (x, y) => predicate(Z.GetCell(x, y)));

			foreach (var pair in flood) {
				yield return Z.GetCell(pair[0], pair[1]);
			}
		}
		/// <summary>
		/// Yield objects in a cardinal flood pattern from origin. Inclusive.
		/// </summary>
		public static IEnumerable<GameObject> LazyCardinalFloodObjects(this Cell cell, Func<GameObject, bool> predicate) {
			var Z = cell.ParentZone;
			var flood = Static.LazyCardinalFlood(cell.X, cell.Y, Z.Width, Z.Height, (x, y) => Z.GetCell(x, y).Objects.Any(predicate));

			foreach (var pair in flood) {
				var floodCell = Z.GetCell(pair[0], pair[1]);
				foreach (var obj in floodCell.Objects) {
					if (!predicate(obj)) continue;
					yield return obj;
				}
			}
		}
		/// <summary>
		/// Check if the enumeration includes any edge of the zone.
		/// </summary>
		public static bool IsOutside(this IEnumerable<Cell> cells) {
			foreach (var C in cells) {
				if (C.X == 0 || C.X >= C.ParentZone.Width - 1)
					return true;
				if (C.Y == 0 || C.Y >= C.ParentZone.Height - 1)
					return true;
			}
			return false;
		}
		/// <summary>
		/// Check if cell has a foreign combat object.
		/// </summary>
		public static bool HasCombatObject(this Cell cell, GameObject observer) {
			var obj = cell.GetFirstObjectWithPart("Combat");
			if (obj == null || obj == observer) return false;
			return true;
		}
		/// <summary>
		/// Check if cell has a foreign combat object.
		/// </summary>
		/*public static Cell GetRandomElement<T>(this HashSet<Cell> list) {
			int count = list.Count;
			if (count == 0) return null;
			
			if (count != 1) {
				Stat.Rand
				if (R == null) {
					R = Stat.Rand;
				}
				return list[Stat.Rand.Next(0, list.Count)];
			}
			return list[0];
		}*/
	}
}
